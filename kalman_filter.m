function [x_filter,P_filter,Kgain] = kalman_filter(A,B,Cc,D,Q,R,P0,x0,p,d);

% Eliz-Mari Lourens
% June 2014

% DISCRETE-TIME STATE-SPACE MODEL:
% x_k+1 = A*x_k + B*p_k + w_k
% d_k = C*x_k + D*p_k + v_k
% Q:    process noise covariance
% R:    measurement noise covariance
% x0:   initial state estimate at t = 0
% P0:   initial error covariance at t = 0
% N:    number of samples
% p:    applied forces [n_p x N]
% d:    data vector [n_d x N]

n_s = size(A,1);	% Number of states
n_d = size(d,1);	% Number of observations
N = size(d,2);		% Number of samples
%x_filter = zeros(n_s,N);		
%P_filter = zeros(n_s,n_s,N);
for time = 1:N
	%	Time update
	if (time==1)
		x_filter(:,time) = A*x0; 												
		P_filter(:,:,time) = A*P0*A' + Q;
	else
		x_filter(:,time) = A*x_filter(:,time-1)+B*p(:,time-1); 												
		P_filter(:,:,time) = A*P_filter(:,:,time-1)*A' + Q;
	end
	
	%	Measurement update
	K_k = P_filter(:,:,time)*Cc'*inv(Cc*P_filter(:,:,time)*Cc'+R);
    Kgain(:,:,time) = K_k;
	x_filter(:,time) = x_filter(:,time) + K_k*(d(:,time)-Cc*x_filter(:,time)-D*p(:,time));
	P_filter(:,:,time) = P_filter(:,:,time) - K_k*Cc*P_filter(:,:,time);
end