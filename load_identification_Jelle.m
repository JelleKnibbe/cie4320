FE_model_Updated;
load("Accelerations_B.mat"); % ACCELERATIONS NOT DISPLACEMENTS!!!!
load("Forces.mat");

n_p = 2;
n_d = 11;

% S_daero = S_d;
S_daero = S_d(1:6,:);
S_dwind = S_d(7:11,:);

% dn = dn - mean(dn,2); % detrend?
% numerical integration?
dwind = dn(7:11,:);
% daero = dn;
daero = dn(1:6,:);

S_pwind = S_p(:,1);
S_paero = S_p(:,2);

dt = 0.01;
N = 2048;
F = 1/dt;
df = F/N;
freq = [0:N/2-1,-N/2:-1]*df;
   
dnw = fft(dn,[],2); 
dwindw = fft(dwind,[],2);
daerow = fft(daero,[],2);
% for ind = 1:n_d
%     integration
%     velo(ind,:) = cumtrapz(detrend(dn(ind,:),2),t);
%     disp(ind,:) = cumtrapz(velo(ind,:),t);
%     dispw(ind,:) = fft(disp(ind,:),[],2);
%     Time domain
%     figure;
%     subplot(2,1,1);
%     plot(t,dn(ind,:)); hold on; 
%     Frequency domain
%     subplot(2,1,2);
%     semilogy(freq(1:N/2),abs(dnw(ind,1:N/2))); hold on;
% end

pw = fft(p,[],2);

% example plots
% figure;
% sgtitle("Column")
% subplot(2,1,1);
% semilogy(freq(1:N/2),abs(dwindw(1,1:N/2)));
% subplot(2,1,2);
% semilogy(freq(1:N/2),abs(dwindw(1,1:N/2)./-(freq(1:N/2).^2)));
% 
% figure;
% sgtitle("Spectra Node 80")
% subplot(2,1,1);
% semilogy(freq(1:N/2),abs(daerow(2,1:N/2))); hold on;
% xlabel("Frequency [Hz]")
% ylabel("Acceleration spectrum")
% xlim([0 15])
% subplot(2,1,2);
% semilogy(freq(1:N/2),abs(daerow(2,1:N/2)./-(freq(1:N/2).^2))); hold on;
% xlabel("Frequency [Hz]")
% ylabel("Displacement spectrum")
% xlim([0 15])


% Construct the damping matrix for proportional damping
xi = 0.02;  % Constant modal damping ratio assumed
Cstar = diag(2*Omega*xi);
C = M*Phi*Cstar*Phi'*M;

L_s=eye(1);

for ind = 1:N/2
    omega = freq(ind)*2*pi;
    fprintf("\nomega: %0.5f: ", omega);
    bwindw = dwindw; % / -(omega)^2;
    baerow = daerow; % / -(omega)^2;
%     b = dnw(:,ind) / (omega)^2;
%     b = dispw(:,ind);
    H = inv(-omega^2*M+1i*omega*C+K) * - omega^2;
%     FRF(ind) = S_paero'*H*S_paero;
    Awind = S_dwind*H*S_pwind;
    Aaero = S_daero*H*S_paero;
%     A = S_d*H*S_p;
%     Condwind(ind) = cond(Awind);
%     Condaero(ind) = cond(Aaero);
%     Cond(ind) = cond(A);
    if freq(ind) > 3.3
        betawind = 0;
        betaaero = 0;
    else
        betawind = 0;
        betaaero = 1e11;
    end
    lambdawind(ind) = betawind*trace(transpose(Awind)*Awind)/trace(transpose(L_s)*L_s);
    lambdaaero(ind) = betaaero*trace(transpose(Aaero)*Aaero)/trace(transpose(L_s)*L_s);
%     lambda(ind) = Cond(ind)*trace(transpose(A)*A)/trace(transpose(L_s)*L_s);
    if freq(ind) > 0
        p_LSw(1,ind) = inv(transpose(Awind)*(Awind)) * transpose(Awind) * bwindw(:,ind);
        p_LSw(2,ind) = inv(transpose(Aaero)*(Aaero)) * transpose(Aaero) * baerow(:,ind);
        p_TIKHw(1,ind) = inv(transpose(Awind)*Awind + lambdawind(ind)^2*transpose(L_s)*L_s)*transpose(Awind)*bwindw(:,ind);
        p_TIKHw(2,ind) = inv(transpose(Aaero)*Aaero + lambdaaero(ind)^2*transpose(L_s)*L_s)*transpose(Aaero)*baerow(:,ind);
%         p_LSw(:,ind) = inv(transpose(A)*(A)) * transpose(A) * b;
%         p_TIKHw(:,ind) = inv(transpose(A)*A + lambda(ind)^2*transpose(L_s)*L_s)*transpose(A)*b;
    else
        p_LSw(1,ind) = 0;
        p_LSw(2,ind) = 0;
        p_TIKHw(1,ind) = 0;
        p_TIKHw(2,ind) = 0;
    end
    fprintf("LS/true: %0.5e", abs(p_LSw(2,ind))/abs(pw(2,ind)));
end

p_LSw = [p_LSw zeros(n_p,1) conj(p_LSw(:,end:-1:2))];
p_LS = ifft(p_LSw,[],2);
% 
p_TIKHw = [p_TIKHw zeros(n_p,1) conj(p_TIKHw(:,end:-1:2))];
p_TIKH = ifft(p_TIKHw,[],2);
% 
% figure;
% semilogy(freq(1:N/2),Cond);
% title("Cond(A)");
% 
% figure;
% semilogy(freq(1:N/2),abs(lambda));
% title("lambda");
% 
% figure;
% semilogy(freq(1:N/2),Condwind);
% title("Cond(Awind)");
% 
% figure;
% semilogy(freq(1:N/2),abs(lambdawind));
% title("lambdawind");
% 
% figure;
% semilogy(freq(1:N/2),Condaero);
% title("Cond(Aaero)");
% 
% figure;
% semilogy(freq(1:N/2),abs(lambdaaero));
% title("lambdaaero");
% 
% figure;
% semilogy(freq(1:N/2),abs(FRF));
% title("FRF of roof node");

for ind = 1:n_p
    % Time domain
    figure;
    subplot(2,1,1);
    sgtitle(force_names(ind))
    plot(t,p_LS(ind,:)); hold on;
    xlabel("time [s]")
    plot(t,p_TIKH(ind,:)); hold on;
    plot(t,p(ind,:),"--"); 
    legend("Least-Squares", "Regularized", "True");
    if ind == 1
        ylabel("Wind load [N/m]")
    elseif ind == 2
        ylabel("Aerobics load [N]")
    end
    xlim([0 20]);
    % Frequency domain
    subplot(2,1,2);
    semilogy(freq(1:N/2),abs(p_LSw(ind,1:N/2))); hold on;
    semilogy(freq(1:N/2),abs(p_TIKHw(ind,1:N/2))); hold on;
    semilogy(freq(1:N/2),abs(pw(ind,1:N/2)));
%     semilogy(freq(1:N/2),abs(p_LSw(ind,1:N/2))./abs(pw(ind,1:N/2)));
    legend("Least-Squares", "Regularized", "True");
    if ind == 1
        ylabel("Wind load Spectrum")
    elseif ind == 2
        ylabel("Aerobics load Spectrum")
    end
    xlim([0 15]);
end