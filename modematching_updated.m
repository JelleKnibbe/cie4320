function [modepairs, Phi_ids, freq_ids, Phi_s, freq_s] = modematching(Phi_id,freq_id ,Phi,freq, S_d)
% Input arguments
% -  Phi_id:  measured/identified modes - normalized
% -  freq_id: measured/identified frequencies [Hz]
% -  Phi:     modelled modes  - normalized
% -  freq:    modelled frequencies [Hz]
%
% Output arguements: 
% -  modepairs: list relating pairs of corresponding modes [identified modelled]
% -  Phi_ids:  sorted identified modes 
% -  freq_ids: sorted identified frequencies
% -  Phi_s:    sorted modelled modes
% -  freq_s:   sorted modelled frequencies
%--------------------------------------------------------------------------

% Indices of measured DOFs in the DOF vector
n_d = size(S_d,1);
for ind = 1:n_d
    ind_d(ind) = find(S_d(ind,:));     
end

% Compute MAC of identified and modelled modes 
MAC =[];
for ind1 = 1:size(Phi_id,2)+5      % Index over computed modes (larger)
    for ind2 = 1:size(Phi_id,2)    % Index over identified modes
       MAC(ind1,ind2)=abs(Phi(ind_d,ind1)'*Phi_id(:,ind2))^2/(norm(Phi(ind_d,ind1))^2*norm(Phi_id(:,ind2))^2);
    end
end
figure();imagesc(MAC); colorbar;

% Set the Treshold for allowing mode pairing
ThMAC = 0.9;    

% Pairing
modepairs = [];                             % preallocate modepairs 
for i = 1:size(MAC,2)                       % for the number of identified modes: 
	if max(MAC(:,i))>=ThMAC                 % if the max MAC in this col is larger than the threshold
		[~, j] = max(MAC(:,i));             % find the largest entry i.e. the best matching computed mode and
		modepairs = [modepairs; [i, j]];    % write info to "modepairs": i: identified mode nr., j: corresponding model mode nr.
	end
end

% Make selection matrices to select pairs of modes
L_id = zeros(size(Phi_id,2),size(modepairs,1));     % preallocate selection matrix size
L    = zeros(size(Phi,2),size(modepairs,1));
for i = 1:size(modepairs,1)
    L_id(modepairs(i,1),i) = 1;     % assemble mode selection matrix: identified modes
    L(modepairs(i,2),i)    = 1;     % assemble mode selection matrix: modelled modes
end

% Re-order / select modes and frequencies: 
Phi_ids  = Phi_id*L_id;                        % selected "re-ordered" identified modes
freq_ids = freq_id(modepairs(:,1));            % and frequencies
Phi_s    = Phi*L;                              % selected "re-ordered" modelled modes
freq_s   = freq(modepairs(:,2));               % and frequencies