% FE_model;
load("Accelerations_A.mat");

% 1 System Identification
% a Sdd
freq = Omega / 2 / pi;
n_d = 35;
N = size(t,2);
dt = t(1,2);
dnw = fft(dn,[],2);

% Segment the time series
nSeg = 5; % number of segments
% Define segmented sampling parameters
N_seg = N/nSeg;
t_seg = [0:N_seg-1]*dt;
F = 1/dt;
df_seg = F/N_seg;
freq_seg =[0:N_seg/2-1,-N_seg/2:-1]*df_seg; 
% Create segmented data vector and transform to the frequency domain
first = 1;
for segment = 1:nSeg
    last = first+N_seg-1;
    d_seg(segment,:,:) = fft(dn(:,first:last),[],2);
    first = last;
end

% Set up the spectral density matrix Sdd and decompose
for ind = 1:N_seg/2
    Sdd = zeros(n_d,n_d);
    for segment = 1:nSeg
       %display(d_seg(segment,:,ind)' * ctranspose(d_seg(segment,:,ind))')
       Sdd = Sdd + d_seg(segment,:,ind)' * ctranspose(d_seg(segment,:,ind)');
    end
    %display(rank(Sdd))
    [U,S,V] = svd(Sdd);  % SVD
    % Store the singular values and vectors at each frequency
    s_value(:,ind) = diag(S);
    U_global(:,:,ind) = U;
end

% b singular values
% Plot singular values of the spectral matrix
figure;
for ind=1:n_d
    semilogy(freq_seg(1:N_seg/2), s_value(ind,1:N_seg/2));
    hold on;
end
%ylim([10^-15 10^15]);
xlabel('Frequency [Hz]');
ylabel('Singular values');