% 1 System Identification
% e MAC matrix

% Calculate and plot the global MAC matrix
eigenvectors;
figure;
for ind1 = 1:9
    for ind2 = 1:9
       MACmatrix(ind1,ind2) = abs(Phi_id(:,ind1)'*Phi_id(:,ind2))^2/ (norm(Phi_id(:,ind1))^2*norm(Phi_id(:,ind2))^2);
    end
end

% figure;
imagesc(MACmatrix); colorbar;
% colorbar;