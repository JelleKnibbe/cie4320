function RunOptim()

%% Brute force assessment
%------------------------

% Define variables and axes names
% x1 = linspace(3000, 8000, 20);
x1 = linspace(5000, 6000, 20);
x2 = linspace(3, 7, 20);
x1txt = 'm_f1';  % Variable name for axis labels                    
x2txt = 'kcf2';

% Define scale factors
sclx1 = max(x1); 
sclx2 = max(x2);

% Assign scale factors to workspace - to allow acces from non-nested-function 
assignin('base','sclx1',sclx1) 
assignin('base','sclx2',sclx2)

% Scale variables - these are scaled back inside ObjFun.m!
x1 = x1./sclx1;
x2 = x2./sclx2;

% Evaluate objective function on grid 
for i = 1:length(x1)
    for j = 1:length(x2) 
        f(i,j) = ObjFun( [x1(i),x2(j)] );     
    end
    display(i)
end

% Call function for plotting ObjFun on grid
plot_brute

%% Gradient based optimization 
%-----------------------------

% Set up shared variables with OUTFUN
history.x = [];
history.fval = [];
 
% Setup optimization: set bounds and initial guess for optimization variables
% lb: lower bounds - scaled
% ub: lower bounds - scaled
% x0: initial guesses - scaled 
lb = [x1(1),x2(1)];    
ub = [x1(end),x2(end)];  
x0 = [5400/sclx1, 5/sclx2];

% Set output function and tolerances
options = optimset('OutputFcn',@outfun);
options.TolX   = 1e-10;
options.TolCon = 1e-8;
 
% fmincon - interior point method - unconstrained - bounded 
[x,fval,exitflag,output,lambda,hessian] = fmincon(@ObjFun,x0,[],[],[],[],lb,ub,[],options);
result = [x(1)*sclx1, x(2)*sclx2, fval]
display(result)

% Assign output to workspace
assignin('base','history',history)
assignin('base','output',output)
assignin('base','oupdate_result',result)

% Call function for plotting results
plot_gradient








% end of main function definition
% --------------------------------------------------------------------------




%% Begin sub function definition
%-------------------------------
function stop = outfun(x,optimValues,state)
    % write intermediate results
    stop = false;  % don't stop
    switch state
        case 'init'
            % do nothing
        case 'iter'
            % during iteration:
            history.fval = [history.fval; optimValues.fval];
            history.x = [history.x; x];
        case 'done'
            % do nothing 
        otherwise
    end
end

function plot_brute()
    % make contour plot and save to visualize output of gradient based method
    h1 = figure();
    contour(x1*sclx1,x2*sclx2,f')
    xlabel(x1txt)
    ylabel(x2txt)
    set(gcf,'Color',[1 1 1])
    savefig(h1,'Brute_Contour.fig')

    % also plot a surface
    h2 = figure();
    [X1,X2] = ndgrid(x1*sclx1,x2*sclx2);
    surfc(X1,X2,f,'FaceAlpha',0.2);
    xlabel(x1txt)
    ylabel(x2txt)
    zlabel('object functional [-]')
    set(gcf,'Color',[1 1 1])
    savefig(h2, 'Brute_Surf.fig')
end

function plot_gradient()
    % plot the results in generated contour and surface plot
    % load plots obtained in previous block
    close all
    openfig('Brute_Contour.fig');       
    hold on 
        plot(x(1)*sclx1,x(2)*sclx2,'ko','MarkerFaceColor','r')
        plot(x0(1)*sclx1,x0(2)*sclx2,'ko','MarkerFaceColor','b')
        plot(history.x(:,1)*sclx1,history.x(:,2)*sclx2);
    hold off; 
    savefig('Brute_Contour.fig')

    openfig('Brute_Surf.fig');
    hold on
        plot3(history.x(:,1)*sclx1,history.x(:,2)*sclx2,history.fval)
        scatter3(history.x(:,1)*sclx1,history.x(:,2)*sclx2,history.fval,'ro','filled')
    hold off
    savefig('Brute_Surf.fig')
end

% end of subfunctions - end of function
%--------------------------------------------------------------------------
 end