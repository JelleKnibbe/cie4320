% Select eigenfrequencies and corresponding eigenvectors from singular 
% value spectrum and accompanying left singular vector matrices and plot

% 1 System Identification
% d eigenvectors

%8 segments, 6 not found and overlapping with 7, 9 and 10 both
% freq1 = 0.513; s(1)=1;
% freq2 = 1.489; s(2)=1;
% freq3 = 3.149; s(3)=1;
% freq4 = 4.980; s(4)=1;
% freq5 = 7.843; s(5)=1;
% freq6 = 8.032; s(6)=2;
% freq7 = 8.325; s(7)=1;
% freq8 = 9.747; s(8)=1;
% freq9 = 10.553; s(9)=2;
% freq10 = 10.687; s(10)=1;

%5 segments, 6 not found, overlap with 7, 9 and 10 same
freq1 = 0.503; s(1)=1;
freq2 = 1.526; s(2)=1;
freq3 = 3.143; s(3)=1;
freq4 = 5.020; s(4)=1;
freq5 = 7.858; s(5)=1;
% freq6 = 8.026; s(6)=2;
freq6 = 8.316; s(6)=1;
freq7 = 9.750; s(7)=1;
freq8 = 10.788; s(8)=1;
freq9 = 10.788; s(9)=2;
% freq11 = 28.549; s(11)=1;
% freq12 = 29.160; s(12)=2;

freq_id = [freq1;freq2;freq3;freq4;freq5;freq6;freq7;freq8;freq9];

indx = [];          
indx(1) =  find(freq_seg>freq1(1),1,'first');
indx(2) =  find(freq_seg>freq2(1),1,'first');
indx(3) =  find(freq_seg>freq3(1),1,'first');
indx(4) =  find(freq_seg>freq4(1),1,'first');
indx(5) =  find(freq_seg>freq5(1),1,'first');
indx(6) =  find(freq_seg>freq6(1),1,'first');
indx(7) =  find(freq_seg>freq7(1),1,'first');
indx(8) =  find(freq_seg>freq8(1),1,'first');
indx(9) =  find(freq_seg>freq9(1),1,'first');
% indx(11) =  find(freq_seg>freq11(1),1,'first');
% indx(12) =  find(freq_seg>freq12(1),1,'first');

% In order to visualize the identified eigenvectors we need to know the 
% coordinates and directions of the measured DOFs. 
% ---------------------------------------------------------------------                        
%S_d = S_dA; 
n_d = size(S_d,1);
for ind = 1:n_d
    ind_d(ind) = find(S_d(ind,:));              % Indices of measured DOFs in the DOF vector
    NodeNr(ind,1) = floor(DOF(ind_d(ind),1));   % Node numbers
    NodeCo(ind,:) = Nodes(NodeNr(ind,1),:);     % Nodal coordinates
end
% Find DOF directions for plotting 
direction = round(rem(S_d*DOF,1)*100);

% Select, normalize and plot: 
% --------------------------------------------
for ind = 1:length(indx) 
    Phi_id(:,ind)  = (U_global(:,s(ind),indx(ind)));
    Phi_id(:,ind)  = Phi_id(:,ind)/norm(Phi_id(:,ind));

    % Relate identified mode shape coordinates to measured DOFs
    for i = 1:n_d
        if direction(i) == 1        % Horizontal measurement
            NodeCo1(i,:) = NodeCo(i,:) + 2 * [0 Phi_id(i,ind) 0 0];      
        elseif direction(i) == 2    % Vertical measurement
            NodeCo1(i,:) = NodeCo(i,:) + 2 * [0  0 Phi_id(i,ind) 0];
        end
    end
    %Plot
    figure; plotelem(Nodes,Elements,Types,'Numbering','off');
    hold on; 
    plotnodes(NodeCo1,'Numbering','off');
%     plotdisp(Nodes,Elements,Types,DOF,Phi(:,ind),'DispMax','off','DispScal',max(Phi(:,ind))/max(NodeCo1(i,:)));
    title(sprintf('Mode %i', ind), sprintf('Identified Frequency: %0.3f Hz', freq_id(ind)), 'FontSize', 12)
end

% Save identified eigendata for model updating step
savefile = 'identified_eigdata.mat';
save(savefile, 'Phi_id','freq_id','ind_d');
