% Horizontal displacement DOF 73
load("Accelerations_B.mat");



c80 = selectdof(DOF,[80.01]);
S_80 = [c80];
u80 = S_80*u_filter;

for ind = 1:(N-1)
    v80(ind) = (u80(ind+1) - u80(ind))/t(2);
end

for ind = 1:(N-2)
    a80(ind )= (v80(ind+1) - v80(ind))/t(2);
end

a80h = fft(a80,[],2);

% 
% figure;
% plot(t,u80);
% title('Horizontal displacement DOF 80');
% xlabel('Time [s]');
% ylabel('Displacement [m]');

t(end)=[];

% figure;
% plot(t,v80);
% title('Horizontal velocity DOF 80');
% xlabel('Time [s]');
% ylabel('Velocity [m/s]');

t(end)=[];

figure;
plot(t,a80);
title('Horizontal acceleration Node 80');
xlabel('Time [s]');
ylabel('Displacement [m]');

load("Accelerations_B.mat");

c80 = selectdof(DOF,[80.02]);
S_80 = [c80];
u80 = S_80*u_filter;

for ind = 1:(N-1)
    v80(ind) = (u80(ind+1) - u80(ind))/t(2);
end

for ind = 1:(N-2)
    a80(ind )= (v80(ind+1) - v80(ind))/t(2);
end

% figure;
% plot(t,u80);
% title('Vertical displacement DOF 80');
% xlabel('Time [s]');
% ylabel('Displacement [m]');

t(end)=[];

% figure;
% plot(t,v80);
% title('Vertical velocity DOF 80');
% xlabel('Time [s]');
% ylabel('Velocity [m/s]');

t(end)=[];

figure;
plot(t,a80);
title('Vertical acceleration Node 80');
xlabel('Time [s]');
ylabel('Acceleration [m/s2]');
% 
% load("Accelerations_B.mat");
% for ind = 1:n_d
%     figure;
%     plot(t,dn(ind,:));
% end

a80v = fft(a80,[],2);  

figure;
plot(freq(1:(N-2)/2),abs(a80h(1,1:(N-2)/2)).*dt);
title('FRF of horizontal acceleration of Node 80');
xlabel('Frequency [Hz]');
ylabel('FRF');
axis([0 20 -inf inf]);
% legend('TIKH','LS','Control');

figure;
plot(freq(1:(N-2)/2),abs(a80v(1,1:(N-2)/2)).*dt);
title('FRF of vertical acceleration of Node 80');
xlabel('Frequency [Hz]');
ylabel('FRF');
axis([0 20 -inf inf]);
% legend('TIKH','LS','Control');