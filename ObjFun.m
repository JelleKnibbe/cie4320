function [f] = ObjFun(x)
% x:    vector containing variables for optimisation 
% new:  This script attempts to circumvent mode mismatching / misidentification
%       by forming pairs of identified & calculated modes based on MAC values 
%       (see 'modematching.m').  
% -------------------------------------------------------------------------

% Get scaling variables from workspace
sclx1 = evalin('base','sclx1');
sclx2 = evalin('base','sclx2');

% Load saved results from identification (as in practical 4) 
% - Phi_id(:,i) : identified i^th mode  
% - freq_id(i)  : identified i^th frequency
% - ind_d       : index
load identified_eigdata.mat

% Calculate Phi and Omega for set of design variables using FE-model
% - Phi(:,i)
% - freq(i)
[Phi,Omega,~,~,~,~] = FE_fun_Jelle([x(1)*sclx1, x(2)*sclx2]);  
freq = Omega/(2*pi);                                  

% Pair calculated modes with measured modes using MAC value:
S_d = evalin('base','S_d');
[modepairs, Phi_ids, freq_ids, Phi_s, freq_s] = modematching(Phi_id, freq_id, Phi, freq, S_d);
nmatch = size(modepairs,1);     % Number of matched modes

%% Compute cost function:
% Formulate a cost function and loop over identified modes/frequencies, use:  
%   freq_ids(i)  : matched identified i^th frequency
%   Phi_ids(:,i) : matched identified i^th mode  
%   freq_s(i)    : matched computed i^th frequency
%   Phi_s(:,i)   : matched computed i^th mode 
%
%   maxnomod     :specify maximum number of modes to be used 
%                   * 1) can be used to limit the number of modes to less than the number of matched modes  
%                   * 2) number of matched modes is the maximum number of usable modes
%                   * 3) if number of matched modes reduces due to
%                   parameter choise to value lower than "maxnomod", then
%                   nmatch = size(modepairs,1) will be used as nomaxmod.
%--------------------------------------------------------------------------

% Define a maxium number of modes to be used
maxnomod = 9;
skipped = 0;
if any(modepairs(:,2)==6)
    disp('model mode 6 paired!')
end
for i = 1:min(maxnomod,nmatch)     % for the number of matched modes or less.
    % Correct for potential sign switching and normalize
    Phi_s(:,i) = Phi_s(:,i)/norm(Phi_s(:,i));     % Extract and normalize mode
    inprod     = Phi_ids(:,i)'*S_d*Phi_s(:,i);    % Compute inner product with identified mode
    Phi_s(:,i) = sign(inprod)*Phi_s(:,i);         % Switch sign if required
    
    % define a cost function 
    T1(i) = norm(S_d*Phi_s(:,i) - Phi_ids(:,i))^2 / norm(Phi_ids(:,i))^2;                                            % contributions modes
    T2(i) = (freq_s(i) - freq_ids(i))^2 / (freq_ids(i)^2);
    
end
% Sum contributions
% Note: In order to keep the cost function smooth, it is important to account
%       for a possibly varying number of modes used.
f = (sum(T1) + sum(T2))/(min(maxnomod,nmatch));









