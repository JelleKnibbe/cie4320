% StaBIL manual
% Tutorial: dynamic analysis: direct time integration: trapezium rule
% Units: m, N

% Assembly of M, K and C
tutorialdyna;
[phi,omega]=eigfem(K,M);             % Calculate eigenmodes and eigenfrequencies
xi=0.07;                             % Damping ratio
nModes=length(K)-size(Constr,1);
C=M.'*phi(:,1:nModes)*diag(2*xi*omega(1:nModes))*phi(:,1:nModes).'*M;  
                                     % Modal -> full damping matrix C

% Sampling parameters
T=2.5;                               % Time window
dt=0.002;                            % Time step
N=T/dt;                              % Number of samples
t=[0:N-1]*dt;                        % Time axis

% Excitation
q=zeros(1,N);                        % Time history (1 * N)
q((t>=0.50) & (t<0.60))=1;           % Time history (1 * N)
p=b*q;                               % Nodal excitation (nDOF * N)

% Direct time integration - trapezium rule
alpha=1/4;
delta=1/2;
theta=1;
u=wilson(M,C,K,dt,p,[alpha delta theta]);

% Figures
figure;
c=selectdof(DOF,[9.01; 13.02; 17.02]);
plot(t,c*u);
title(['Nodal response (direct time integration)']);
xlabel('Time [s]');
xlim([0 4.1]) 
ylabel('Nodal displacements [m]');
legend('9.01','13.02','17.02');

% Movie
figure;
animdisp(Nodes,Elements,Types,DOF,u);

% Display
disp('Maximum nodal response 9.01 13.02 17.02');
disp(max(abs(c*u),[],2));
