% Stress concentration around a circular hole in a disk.
r = 1;                           % Radius of hole [m]
L = 20;                          % Width of plate [m]
p = 1;                           % Uniform pressure [N/m^2]
n = 40;                          % Mesh parameter (nElem = m*n)
m = 20;                          % Mesh parameter
Types = {1 'shell8'};            % {EltTypID EltName} 
Sections = [1 1];                % [SecID t] 
Materials = [1 200000 0];      % [MatID E nu]



% define lines
Line1 = [r 0 0;L/2 0 0];
Line2 = [L/2 0 0;L/2 L/2 0;0 L/2 0];
Line3 = [0 L/2 0;0 r 0];
Line4 = [r*sin((0:5).'*pi/10) r*cos((0:5).'*pi/10) zeros(6,1)];

[Nodes,Elements,Edge1,Edge2,Edge3,Edge4] = makemesh(Line1,Line2,Line3,Line4,...
               m,n,Types(1,:),Sections(1,1),Materials(1,1),'L2method','linear');

% Check mesh:
figure;
plotnodes(Nodes,'numbering','off');
hold('on')
plotelem(Nodes,Elements,Types,'numbering','off');
title('Nodes and elements');

% Select all dof:
DOF = getdof(Elements,Types);
% Apply boundary conditions:
% - Line1 and Line3: symmetry condition
seldof = [0.03;0.04;0.05;0.06;Edge1+0.02;Edge3+0.01];
DOF = removedof(DOF,seldof);

% Assemble K:
K = asmkm(Nodes,Elements,Types,Sections,Materials,DOF);

% Apply load to upper edge (equivalent nodal forces):
C = selectdof(DOF,Edge2(1:(end-1)/2+1)+0.02); 
P = C.'*[1/6; repmat([2/3; 1/3],((length(Edge2)-1)/2-2)/2,1);2/3; 1/6]*p*L/m;
U = K\P;

% Calculate stress in cylindrical global coordinate system
[SeGCS]=elemstress(Nodes,Elements,Types,Sections,Materials,DOF,U,'gcs','cyl');

% Get nodal stress from element solution
[SnGCS] = nodalstress(Nodes,Elements,Types,SeGCS);

% plot results
figure;
plotstresscontourf('sx',Nodes,Elements,Types,SnGCS)
title('\sigma_{r}')
figure;
plotstresscontourf('sy',Nodes,Elements,Types,SnGCS)
title('\sigma_{\theta}')
figure;
plotstresscontourf('sxy',Nodes,Elements,Types,SnGCS)
title('\sigma_{r\theta}')