% StaBIL manual
% Tutorial: dynamic analysis: modal superposition: transform to f-dom
% Units: m, N

% Assembly of M, K and C
tutorialdyna;
[phi,omega]=eigfem(K,M);        % Calculate eigenmodes and eigenfrequencies
xi=0.07;                        % Damping ratio
nModes=length(K)-size(Constr,1);
C=M.'*phi(:,1:nModes)*diag(2*xi*omega(1:nModes))*phi(:,1:nModes).'*M;  
                                % Modal -> full damping matrix C

% Sampling parameters
N=2048;                         % Number of samples
dt=0.002;                       % Time step
T=N*dt;                         % Period
F=N/T;                          % Sampling frequency
df=1/T;                         % Frequency resolution
t=[0:N-1]*dt;                   % Time axis
f=[0:N/2-1]*df;                 % Positive frequencies corresponding to FFT [Hz]
Omega=2*pi*f;                   % Idem [rad/s]

% Excitation
q=zeros(1,N);                   % Time history (1 * N)
q((t>=0.50) & (t<0.60))=1;      % Time history (1 * N)
Q=fft(q);                       % Frequency content (1 * N)
Q=Q(1:N/2);                     % Frequency content, positive freq (1 * N/2)
Pd=b*Q;                        % Nodal excitation, positive freq (nDOF * N/2)

% Solution for each frequency
Ud=zeros(size(Pd));
for k=1:N/2
    Kd=-Omega(k)^2*M+Omega(k)*i*C+K;
    Ud(:,k)=Kd\Pd(:,k);
end

% F-dom -> t-dom
Ud=[Ud, zeros(length(K),1), conj(Ud(:,end:-1:2))];
u=ifft(Ud,[],2);               % Nodal response (nDOF * N)

% Figures
figure;
c=selectdof(DOF,[9.01; 13.02; 17.02]);
plot(t,c*u);
title('Nodal response (direct method in f-dom)');
xlabel('Time [s]');
xlim([0 4.1])
ylabel('Displacement [m]');
legend('9.01','13.02','17.02');

% Movie
figure;
animdisp(Nodes,Elements,Types,DOF,u);

% Display
disp('Maximum nodal response 9.01 13.02 17.02');
disp(max(abs(c*u),[],2));
