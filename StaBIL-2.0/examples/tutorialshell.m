% A BARREL VAULT ROOF SUBJECTED TO ITS SELF WEIGHT
% Reference: COOK, CONCEPTS AND APPL OF F.E.A., 2ND ED., 1981, PP. 284-287.

%% Parameters
R=25;               % Radius of cylindrical roof 
L=50;               % Length of cylindrical roof 
t=0.25;             % Thickness of roof 
theta = 40*pi/180;  % Angle of cylindrical roof 
E = 4.32*10^8;      % Youngs modulus
nu = 0;             % Poisson coefficient  
rho = 36.7347;      % Density
N = 8;              % Number of elements

%% Mesh

% Lines = [x1 y1 z1;x2 y2 z2;...]
Line1 = [0 0 0;0 0 L/2];
Line2 = [R*sin(theta*(0:0.1:1).') R*cos(theta*(0:0.1:1).')-R L*ones(11,1)/2];
Line3 = [R*sin(theta) R*cos(theta)-R L/2; R*sin(theta) R*cos(theta)-R 0]; 
Line4 = [R*sin(theta*(1:-0.1:0).') R*cos(theta*(1:-0.1:0).')-R zeros(11,1)];

% Specify element type for mesh
Materials = [1 E nu rho];
Sections = [1 t];
Types = {1 'shell8'};

% Mesh the area between lines 1,2,3,4 with N * N elements of type shell8,
% section number 1 and material number 1.
[Nodes,Elements,Edge1,Edge2,Edge3,Edge4] = ...
    makemesh(Line1,Line2,Line3,Line4,N,N,Types(1,:),1,1);

% Check mesh:
figure;
plotnodes(Nodes,'numbering','off');
hold('on')
plotelem(Nodes,Elements,Types,'numbering','off');
title('Nodes and elements');

%% Assemble stiffness matrix

% Select all dof:
DOF = getdof(Elements,Types);

% Apply boundary conditions:
% - Line1 and Line4: symmetry condition
% - Line2: simply supported
sdof = [Edge1+0.01;Edge1+0.06;Edge1+0.05;Edge2+0.02;Edge2+0.01;
        Edge2+0.06;Edge4+0.03;Edge4+0.04;Edge4+0.05];
DOF = removedof(DOF,sdof);

% Assemble K:
K = asmkm(Nodes,Elements,Types,Sections,Materials,DOF);

%% Solution

% Apply gravitational acceleration and determine equivalent nodal forces:
DLoads=accel([0 9.8 0],Elements,Types,Sections,Materials);
P=elemloads(DLoads,Nodes,Elements,Types,DOF);


% Solve K * U = P:
U = K\P;

% Plot displacements:
figure;
plotdisp(Nodes,Elements,Types,DOF,U)
title('Displacements')

% Check target displacement:
TP1 = selectdof(DOF,intersect(Edge3,Edge4)+0.02);
Utp1 = TP1*U
ratio_u = -Utp1/0.3016

%% Stress

% Determine element stress in global and local(element) coordinate system:
[SeGCS,SeLCS,vLCS]=elemstress(Nodes,Elements,Types,Sections,Materials,DOF,U);

% print stress:
printstress(Elements,SeGCS)

% plot stress contour:
figure;
plotstresscontour('sx',Nodes,Elements,Types,SeGCS,'location','bot')
title('sx in gcs (element solution)')

% plot filled contours:
figure;
plotstresscontourf('sx',Nodes,Elements,Types,SeGCS,'location','bot')
title('sx in gcs (element solution)')

figure;
plotstresscontour('sx',Nodes,Elements,Types,SeLCS,'location','bot')
title('sx in lcs')

% plot lcs for shell elements
figure;
plotlcs(Nodes,Elements,Types,vLCS)
title('local coordinate system')

% Calculate nodal solution
% SnGCS:  stress arranged per element
% SnGCS2: stress arranged per node 
[SnGCS,SnGCS2]=nodalstress(Nodes,Elements,Types,SeGCS);
[SnLCS,SnLCS2]=nodalstress(Nodes,Elements,Types,SeLCS);
figure;
plotstresscontour('sx',Nodes,Elements,Types,SnGCS,'location','bot');
title('sx in gcs (nodal solution)')

% Stress ratios:

ratio_sz = SnGCS2(intersect(Edge3,Edge4),16)/358420
ratio_st = SnGCS2(intersect(Edge4,Edge1),14)/(-213400)

%% Shell forces

% Shell forces (element solution):
[FeLCS]=elemshellf(Elements,Sections,SeLCS);
figure;
plotshellfcontour('my',Nodes,Elements,Types,FeLCS)
title('my (element solution)')

% Nodal solution:
[FnLCS,FnLCS2]=nodalshellf(Nodes,Elements,Types,FeLCS);
figure;
plotshellfcontour('my',Nodes,Elements,Types,FnLCS)
title('my (nodal solution)')

%% Principal stress

% Principal stresses:
[Spr,Vpr]=principalstress(Elements,SnGCS);
figure;
plotstresscontour('s1',Nodes,Elements,Types,Spr,'location','bot');
title('s1')
% plot principal stresses:
figure;
plotprincstress(Nodes,Elements,Types,Spr,Vpr)
title('principal stresses')


