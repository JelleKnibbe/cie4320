% StaBIL manual
% Tutorial: dynamic analysis: modal superposition: piecewise exact integration
% Units: m, N

% Assembly of M and K
tutorialdyna;

% Sampling parameters
T=2.5;                          % Time window
dt=0.002;                       % Time step
N=T/dt;                         % Number of samples
t=[0:N-1]*dt;                   % Time axis

% Eigenvalue analysis
nMode=12;                       % Number of modes to take into account
[phi,omega]=eigfem(K,M,nMode);  % Calculate eigenmodes and eigenfrequencies
xi=0.07;                        % Constant modal damping ratio

% Excitation
bm=phi.'*b;                     % Spatial distribution, modal (nMode * 1)
q=zeros(1,N);                   % Time history (1 * N)
q((t>=0.50) & (t<0.60))=1;      % Time history (1 * N)
pm=bm*q;                        % Modal excitation (nMode * N)

% Modal analysis
x=msupt(omega,xi,t,pm,'zoh');

% Modal displacements -> nodal displacements
u=phi*x;                        % Nodal response (nDOF * N)

% Figures
figure;
plot(t,x);
title('Modal response (piecewise linear exact integration)');
xlabel('Time [s]');
xlim([0 4.1]) 
ylabel('Displacement [m kg^{0.5}]');
legend([repmat('Mode ',nMode,1) num2str([1:nMode].')]);

figure;
c=selectdof(DOF,[9.01; 13.02; 17.02]);
plot(t,c*u);
title('Nodal response (piecewise linear exact integration)');
xlabel('Time [s]');
xlim([0 4.1]) 
ylabel('Displacement [m]');
legend('9.01','13.02','17.02');

% Movie
figure;
animdisp(Nodes,Elements,Types,DOF,u);

% Display
disp('Maximum modal response');
disp(max(abs(x),[],2));

disp('Maximum nodal response 9.01 13.02 17.02');
disp(max(abs(c*u),[],2));
