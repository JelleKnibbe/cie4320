% StaBIL manual
% Example 4
% Units: m, kN

% Types={EltTypID EltName}
Types=  {1        'beam';
         2        'truss'};

% Sections
bCol=0.2;  % Column section width
hCol=0.3;  % Column section height
bBeam=0.2; % Beam section width
hBeam=0.5; % Beam section height
Atruss=0.001;

% Sections=[SecID A           ky  kz  Ixx                 Iyy              Izz              yt      yb       zt     zb]
Sections=  [1     hCol*bCol   Inf Inf 0.196*bCol^3*hCol   bCol^3*hCol/12   hCol^3*bCol/12   hCol/2  hCol/2  bCol/2  bCol/2;   % Columns
            2     hBeam*bBeam Inf Inf 0.249*bBeam^3*hBeam bBeam^3*hBeam/12 hBeam^3*bBeam/12 hBeam/2 hBeam/2 bBeam/2 bBeam/2; % Beams
            3     Atruss      NaN NaN NaN                 NaN              NaN              NaN     NaN     NaN     NaN];

% Materials=[MatID E      nu      rho];
Materials=  [1     35e9   0.2     2500;   %concrete
             2     210e9  0.3     7850];  %steel

L=5;
H=3.5;
B=4;

% Nodes=[NodID X  Y  Z]
Nodes=  [1     0  0  0;
         2     L  0  0]      
Nodes=reprow(Nodes,1:2,2,[2 0 0 H])
Nodes=[Nodes; 
         10    2  0  2]                                         % reference node
Nodes=reprow(Nodes,1:7,2,[100 0 B 0])

figure
plotnodes(Nodes);

% Elements=[EltID TypID SecID MatID n1 n2 n3]
Elements=[ 1      1     1     1     1  3  10;
           2      1     1     1     2  4  10];
Elements=reprow(Elements,1:2,1,[2 0 0 0 2 2 0])
Elements=[ Elements;
           5      1     2     1     3  4  10; 
           6      1     2     1     5  6  10];   
Elements=reprow(Elements,1:6,2,[100 0 0 0 100 100 100]) 
Elements=[ Elements;
          301     2     3     2     3  103 NaN; 
          302     2     3     2     4  104 NaN];     
Elements=reprow(Elements,19:20,1,[2 0 0 0 2 2 0]) 
Elements=reprow(Elements,19:22,1,[4 0 0 0 100 100 0]) 
Elements=[ Elements;
          309     2     3     2      4 106 NaN; 
          310     2     3     2      6 104 NaN];           

hold('on');
plotelem(Nodes,Elements,Types);
title('Nodes and elements');

% Plot elements in different colors in order to check the section definitions
figure
plotelem(Nodes,Elements(find(Elements(:,3)==1),:),Types,'r');
hold('on');
plotelem(Nodes,Elements(find(Elements(:,3)==2),:),Types,'g');
plotelem(Nodes,Elements(find(Elements(:,3)==3),:),Types,'b');
title('Elements: sections')

% Degrees of freedom
DOF=getdof(Elements,Types);

% Boundary conditions: hinges 
seldof=[  1.01;   1.02;   1.03;   2.01;   2.02;   2.03; 
        101.01; 101.02; 101.03; 102.01; 102.02; 102.03; 
        201.01; 201.02; 201.03; 202.01; 202.02; 202.03;];

DOF=removedof(DOF,seldof);

% Assembly of stiffness matrix K 
K=asmkm(Nodes,Elements,Types,Sections,Materials,DOF);

% Loads

% Own weight
DLoadsOwn=accel([0 0 9.81],Elements,Types,Sections,Materials);

% Wind load

% DLoads=[EltID n1globalX n1globalY n1globalZ ...]
DLoadsWind =[1 0 0    0 0 1500 0;
             2 0 0    0 0 1500 0;
             3 0 1500 0 0 1500 0;
             4 0 1500 0 0 1500 0];
             
DLoads=multdloads(DLoadsOwn,DLoadsWind);

P=elemloads(DLoads,Nodes,Elements,Types,DOF);

% Solve K * U = P 
U=K\P;

figure
plotdisp(Nodes,Elements,Types,DOF,U(:,1),DLoads(:,:,1),Sections,Materials)
title('Displacements: own weight')

figure
plotdisp(Nodes,Elements,Types,DOF,U(:,2),DLoads(:,:,2),Sections,Materials)
title('Displacements: wind')

% Compute forces
[ForcesLCS,ForcesGCS]=elemforces(Nodes,Elements,Types,Sections,Materials,DOF,U,DLoads);

% Compute reaction forces for load case 1
Freac=reaction(Elements,ForcesGCS(:,:,1),[1.03; 2.03; 101.03; 102.03; 201.03; 202.03])

% Plot element forces for load case 1
figure
plotforc('norm',Nodes,Elements,Types,ForcesLCS(:,:,1),DLoads(:,:,1))
title('Normal forces: Own weight')
figure
plotforc('sheary',Nodes,Elements,Types,ForcesLCS(:,:,1),DLoads(:,:,1))
title('Shear forces along y: Own weight')
figure
plotforc('shearz',Nodes,Elements,Types,ForcesLCS(:,:,1),DLoads(:,:,1))
title('Shear forces along z: Own weight')
figure
plotforc('momx',Nodes,Elements,Types,ForcesLCS(:,:,1),DLoads(:,:,1))
title('Torsional moments: Own weight')
figure
plotforc('momy',Nodes,Elements,Types,ForcesLCS(:,:,1),DLoads(:,:,1))
title('Bending moments around y: Own weight')
figure
plotforc('momz',Nodes,Elements,Types,ForcesLCS(:,:,1),DLoads(:,:,1))
title('Bending moments around z: Own weight')

% Plot element forces for load case 2
figure
plotforc('norm',Nodes,Elements,Types,ForcesLCS(:,:,2),DLoads(:,:,2))
title('Normal forces: Wind')
figure
plotforc('sheary',Nodes,Elements,Types,ForcesLCS(:,:,2),DLoads(:,:,2))
title('Shear forces along y: Wind')
figure
plotforc('shearz',Nodes,Elements,Types,ForcesLCS(:,:,2),DLoads(:,:,2))
title('Shear forces along z: Wind')
figure
plotforc('momx',Nodes,Elements,Types,ForcesLCS(:,:,2),DLoads(:,:,2))
title('Torsional moments: Wind')
figure
plotforc('momy',Nodes,Elements,Types,ForcesLCS(:,:,2),DLoads(:,:,2))
title('Bending moments around y : Wind')
figure
plotforc('momz',Nodes,Elements,Types,ForcesLCS(:,:,2),DLoads(:,:,2))
title('Bending moments around z: Wind')

% Load combinations

% Safety factors
gamma_own=1.35;
gamma_wind=1.5;

% Combination factors
psi_wind=1;

% Load combination
U_UGT=gamma_own*U(:,1)+gamma_wind*psi_wind*U(:,2);
Forces_UGT=gamma_own*ForcesLCS(:,:,1)+gamma_wind*psi_wind*ForcesLCS(:,:,2);
DLoads_UGT(:,1)=DLoads(:,1,1)
DLoads_UGT(:,2:7)=gamma_own*DLoads(:,2:7,1)+gamma_wind*psi_wind*DLoads(:,2:7,2);

figure
plotdisp(Nodes,Elements,Types,DOF,U_UGT,DLoads_UGT,Sections,Materials)

printdisp(Nodes,DOF,U_UGT);

printforc(Elements,Forces_UGT);

% Plot element forces
figure
plotforc('norm',Nodes,Elements,Types,Forces_UGT,DLoads_UGT)
title('Normal forces: UGT')
figure
plotforc('sheary',Nodes,Elements,Types,Forces_UGT,DLoads_UGT)
title('Shear forces along y: UGT')
figure
plotforc('shearz',Nodes,Elements,Types,Forces_UGT,DLoads_UGT)
title('Shear forces along z: UGT')
figure
plotforc('momx',Nodes,Elements,Types,Forces_UGT,DLoads_UGT)
title('Torsional moments: UGT')
figure
plotforc('momy',Nodes,Elements,Types,Forces_UGT,DLoads_UGT)
title('Bending moments around y: UGT')
figure
plotforc('momz',Nodes,Elements,Types,Forces_UGT,DLoads_UGT)
title('Bending moments around z: UGT')

% Plot stresses
figure
plotstress('snorm',Nodes,Elements,Types,Sections,Forces_UGT,DLoads_UGT)
title('Normal stresses due to normal forces')
figure
plotstress('smomyt',Nodes,Elements,Types,Sections,Forces_UGT,DLoads_UGT)
title('Normal stresses due to bending moments around y: top')
figure
plotstress('smomyb',Nodes,Elements,Types,Sections,Forces_UGT,DLoads_UGT)
title('Normal stresses due to bending moments around y: bottom')
figure
plotstress('smomzt',Nodes,Elements,Types,Sections,Forces_UGT,DLoads_UGT)
title('Normal stresses due to bending moments around z: top')
figure
plotstress('smomzb',Nodes,Elements,Types,Sections,Forces_UGT,DLoads_UGT)
title('Normal stresses due to bending moments around z: bottom')
figure
plotstress('smax',Nodes,Elements,Types,Sections,Forces_UGT,DLoads_UGT)
title('Maximal normal stresses')
figure
plotstress('smin',Nodes,Elements,Types,Sections,Forces_UGT,DLoads_UGT)
title('Minimal normal stresses')
