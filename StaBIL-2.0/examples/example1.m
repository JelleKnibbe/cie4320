% StaBIL manual
% Example 1
% Units: m, kN

% Nodes=[NodID X  Y  Z]
Nodes=  [1     0  0  0;
         2     0  4  0;
         3     0  4  0;
         4     4  4  0;
         5     6  4  0;
         6     6  3  0;
         7     7  3  0;
         8     6  0  0;        
         9     2  2  0];          % reference node

% Check the node coordinates as follows:     
figure
plotnodes(Nodes);

% Element types -> {EltTypID EltName}
Types=  {1        'beam'};

b=0.20;
h=0.40;

% Sections=[SecID A      ky   kz   Ixx Iyy Izz]
Sections=  [1     b*h    Inf  Inf  0   0   b*h^3/12];

% Materials=[MatID E nu];
Materials=  [1      30e6 0.2];              % concrete

% Elements=[EltID TypID SecID MatID n1 n2 n3]
Elements=  [1     1     1     1     1  2  9;              
            2     1     1     1     3  4  9;
            3     1     1     1     4  5  9;
            4     1     1     1     5  6  9;              
            5     1     1     1     6  7  9;
            6     1     1     1     6  8  9];

% Check element definitions as follows:
hold('on');
plotelem(Nodes,Elements,Types);
title('Nodes and elements');

% Degrees of freedom 
% Assemble a column matrix containing all DOFs at which stiffness is
% present in the model:
DOF=getdof(Elements,Types);

% Remove all DOFs equal to zero from the vector:
%  - 2D analysis: select only UX,UY,ROTZ
%  - clamp node 1
%  - remove the horizontal displacement at node 8
seldof=[0.03; 0.04; 0.05; 1.00; 8.01];
DOF=removedof(DOF,seldof);

% Assembly of stiffness matrix K 
K=asmkm(Nodes,Elements,Types,Sections,Materials,DOF);

% Nodal loads: -20 kN vertically on node 4
%              -1 kNm on node 7.
seldof=[4.02; 7.06];
PLoad=[-20; -1];

% Assembly of the load vectors:
P=nodalvalues(DOF,seldof,PLoad);

% Constraint equations: Constant=Coef1*DOF1+Coef2*DOF2+ ...
% Constraints=[Constant  Coef1 DOF1  Coef2 DOF2 ...]
% Imposed displacement -0.02 m vertically in node 8.
Constr=       [0         1     2.01  -1    3.01;
               0         1     2.02  -1    3.02;
              -0.02      1     8.02   NaN    NaN];
 
% Add constraint equations
[K,P]=addconstr(Constr,DOF,K,P);

% Solve K * U = P 
U=K\P;

% Plot displacements
figure
plotdisp(Nodes,Elements,Types,DOF,U)

% The displacements can be displayed as follows:
printdisp(Nodes,DOF,U);

% Compute element forces
Forces=elemforces(Nodes,Elements,Types,Sections,Materials,DOF,U);

% The element forces can be displayed in a orderly table:
printforc(Elements,Forces);

% Plot element forces
figure
plotforc('norm',Nodes,Elements,Types,Forces)
title('Normal forces')

figure
plotforc('sheary',Nodes,Elements,Types,Forces)
title('Shear forces')

figure
plotforc('momz',Nodes,Elements,Types,Forces)
title('Bending moments')
