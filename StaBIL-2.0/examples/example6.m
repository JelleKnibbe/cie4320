% dynamic plate problem (dkt element)

% parameters

Lx = 10;                    % length x-direction
Ly = 10;                    % length y-direction
t = 1;                      % thickness plate
E = 200000;                 % E-modulus
nu = 0.3;                   % Poisson-coeff.
rho = 7000;                 % mass density
m = 20;                     % number of elements in x-direction
n = 20;                     % number of elements in y-direction
Types = {1 'shell4'};       % {EltTypID EltName} 
Sections = [1 t];           % [SecID t] 
Materials = [1 E nu rho];   % [MatID E nu]


% mesh

Line1 = [0 0 0;Lx 0 0];
Line2 = [Lx 0 0;Lx Ly 0];
Line3 = [Lx Ly 0;0 Ly 0];
Line4 = [0 Ly 0;0 0 0];

[Nodes,Elements,Edge1,Edge2,Edge3,Edge4] = makemesh(Line1,Line2,Line3,Line4,n,m,Types(1,:),1,1);
figure;
plotnodes(Nodes);
figure;
plotelem(Nodes,Elements,Types);

% DOFs (simply supported plate)

DOF = getdof(Elements,Types);
sdof = [0.01;0.02;0.06;[Edge1;Edge2;Edge3;Edge4]+0.03;[Edge1;Edge3]+0.05;[Edge2;Edge4]+0.04]; 
DOF = removedof(DOF,sdof);

% K & M

[K,M] = asmkm(Nodes,Elements,Types,Sections,Materials,DOF);

% eigenmodes

nMode = 10;       
[phi,omega] = eigfem(K,M,nMode);
figure;
animdisp(Nodes,Elements,Types,DOF,phi(:,1));

% analytical solution

[mm,nn] = meshgrid((1:m),(1:n));
aomega = sqrt(E*t^2/(12*(1-nu^2)*rho))*((mm*pi/Lx).^2+(nn*pi/Ly).^2);
aomega = reshape(aomega,numel(aomega),1);
aomega = sort(aomega);
ratio = omega./aomega(1:nMode)






