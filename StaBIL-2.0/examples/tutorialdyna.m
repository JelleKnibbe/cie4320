% StaBIL manual
% Tutorial: dynamic analysis: model
% Units: m, N

L=4;
H=4;
nElemCable=8;

% Nodes=[NodID X  Y  Z]
Nodes=  [1     0  0  0;
         2     L  0  0];
Nodes=reprow(Nodes,1:2,4,[2 0 H/4 0]);
Nodes=  [Nodes; 
         11    0  H  0];       
Nodes=reprow(Nodes,11,3,[1 L/4 0 0]); 
Nodes=  [Nodes; 
         15    1  5  0];          % reference node
Nodes=  [Nodes; 
         16    0  0  0];       
Nodes=reprow(Nodes,16,nElemCable,[1 L/nElemCable H/nElemCable 0]);  

% Check the node coordinates as follows:     
figure
plotnodes(Nodes);

% Element types -> {EltTypID EltName}
Types=  {1        'beam'};

b=0.10;
h=0.25;
r=0.004;

% Sections=[SecID A      ky   kz   Ixx Iyy Izz]
Sections=  [1     b*h    Inf  Inf  0   0   b*h^3/12;
            2     pi*r^2 Inf  Inf  0   0   pi*r^4/4];

% Materials=[MatID E nu];
Materials=  [1      30e9 0.2 2500;               % concrete
             2     210e9 0.3 7850];              % steel

% Elements=[EltID TypID SecID MatID n1 n2 n3]
Elements=  [1     1     1     1     1  3  15;              
		        2     1     1     1     2  4  15];
Elements=reprow(Elements,1:2,3,[2 0 0 0 2 2 0]);
Elements=[ Elements;
           9      1     1     1     11 12 15; 
           10     1     1     1     12 13 15;
           11     1     1     1     13 14 15;
           12     1     1     1     14 10 15;
           13     1     2     2     16 17 15];  
Elements=reprow(Elements,13,(nElemCable-1),[1 0 0 0 1 1 0]); 	        

% Check node and element definitions as follows:
hold('on');
plotelem(Nodes,Elements,Types);
title('Nodes and elements');

% Degrees of freedom
DOF=getdof(Elements,Types);

% Boundary conditions 
seldof=[0.03; 0.04; 0.05; 1.01; 1.02; 1.06; 2.01; 2.02; 16.01; 16.02];

DOF=removedof(DOF,seldof);

% Assembly of stiffness matrix K 
[K,M]=asmkm(Nodes,Elements,Types,Sections,Materials,DOF);

% DLoads=[EltID n1globalX n1globalY n1globalZ ...]
DLoads=[1 2000 0 0 2000 0 0;
        3 2000 0 0 2000 0 0;
        5 2000 0 0 2000 0 0;
        7 2000 0 0 2000 0 0];
b=elemloads(DLoads,Nodes,Elements,Types,DOF); % Spatial distribution, nodal (nDOF * 1)

% Constraint equations: Constant=Coef1*DOF1+Coef2*DOF2+ ...
% Constraints=[Constant  Coef1 DOF1  Coef2 DOF2 ...]
Constr=       [0         1     9.01  -1    11.01;
               0         1     9.02  -1    11.02;
               0         1     10.01 -1    (16.01+nElemCable);
               0         1     10.02 -1    (16.02+nElemCable)];
               
[K,b,M]=addconstr(Constr,DOF,K,b,M);
