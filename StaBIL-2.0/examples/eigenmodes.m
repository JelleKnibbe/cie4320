% StaBIL manual
% Tutorial: dynamic analysis: eigenvalue problem
% Units: m, N

% Assembly of M and K
tutorialdyna;

% Eigenvalue problem
nMode=12; 
[phi,omega]=eigfem(K,M,nMode);

% Display eigenfrequenties
disp('Lowest eigenfrequencies [Hz]');
disp(omega/2/pi);

% Plot eigenmodes
figure;
plotdisp(Nodes,Elements,Types,DOF,phi(:,1),'DispMax','off')
figure;
plotdisp(Nodes,Elements,Types,DOF,phi(:,2),'DispMax','off')
figure;
plotdisp(Nodes,Elements,Types,DOF,phi(:,5),'DispMax','off')
figure;
plotdisp(Nodes,Elements,Types,DOF,phi(:,8),'DispMax','off')
figure;
plotdisp(Nodes,Elements,Types,DOF,phi(:,11),'DispMax','off')
figure;
plotdisp(Nodes,Elements,Types,DOF,phi(:,12),'DispMax','off')

% Animate eigenmodes
figure;
animdisp(Nodes,Elements,Types,DOF,phi(:,1))
title('Eigenmode 1')

figure;
animdisp(Nodes,Elements,Types,DOF,phi(:,2))
title('Eigenmode 2')

figure;
animdisp(Nodes,Elements,Types,DOF,phi(:,5))
title('Eigenmode 5')

figure;
animdisp(Nodes,Elements,Types,DOF,phi(:,8))
title('Eigenmode 8')

figure;
animdisp(Nodes,Elements,Types,DOF,phi(:,11))
title('Eigenmode 11')

figure;
animdisp(Nodes,Elements,Types,DOF,phi(:,12))
title('Eigenmode 12')
