% StaBIL manual
% Example 3
% Units: m, kN

% Nodes=[NodID X     Y  Z]
Nodes=  [1     2.5   0  0;
         2     2.5   5  0;
         3     2.5   5  2.5;
         4     2.5   0  2.5;
         5     1.25  0  3.75;
         6     1.25  5  3.75;
         7     0     0  2.5;
         8     0     5  2.5;
         9     0     0  0;
         10    0     5  0];          % reference node

% Check the node coordinates as follows:     
figure
plotnodes(Nodes);

% Element types -> {EltTypID EltName}
Types=  {1        'beam'};

b=0.1;
h=0.2;

% Sections=[SecID A    ky   kz   Ixx          Iyy       Izz]
Sections=  [1     b*h  Inf  Inf  0.229*h/b^3  h*b^3/12  b*h^3/12];

% Materials=[MatID E    nu];
Materials=  [1     30e6 0.2];              % concrete

% Elements=[EltID TypID SecID MatID n1 n2 n3]
Elements=  [1     1     1     1     1  2  3;              
            2     1     1     1     2  10 3;
            3     1     1     1     10 9  8;
            4     1     1     1     9  1  5;
            5     1     1     1     1  4  3;
            6     1     1     1     2  3  4;              
            7     1     1     1     10 8  7;
            8     1     1     1     9  7  8;
            9     1     1     1     3  4  2;
            10    1     1     1     3  6  10;
            11    1     1     1     8  6  10;              
            12    1     1     1     5  6  8;
            13    1     1     1     4  5  9;
            14    1     1     1     7  5  9;
            15    1     1     1     7  8  10];

% Check node and element definitions as follows:
hold('on');
plotelem(Nodes,Elements,Types);
title('Nodes and elements');

DOF=getdof(Elements,Types);

% Remove all DOFs equal to zero from the vector:
% clamp nodes 1, 2, 9 and 10
seldof=[1.00; 2.00; 9.00; 10.00];
DOF=removedof(DOF,seldof);

% Assembly of stiffness matrix K 
K=asmkm(Nodes,Elements,Types,Sections,Materials,DOF);

% Nodal loads: load case 1: 6 kN horizontally on nodes 4, 7, 5
%              load case 2: 15 kN horizontally on nodes 7
seldof=[4.02; 7.02; 5.02];
PLoad=[6 0;
       6 15;
       6 0];

% Assembly of the load vectors:
P=nodalvalues(DOF,seldof,PLoad);

% Solve K * U = P 
U=K\P;

% Plot displacements
figure
plotdisp(Nodes,Elements,Types,DOF,U)
title('Displacements')

% The displacements can be displayed as follows:
printdisp(Nodes,DOF,U(:,1));
printdisp(Nodes,DOF,U(:,2));

% Compute element forces
Forces=elemforces(Nodes,Elements,Types,Sections,Materials,DOF,U);

% The element forces can be displayed in a orderly table:
printforc(Elements,Forces(:,:,1));
printforc(Elements,Forces(:,:,2));

% Plot element forces
figure
plotforc('norm',Nodes,Elements,Types,Forces(:,:,1))
title('Normal forces')

figure
plotforc('sheary',Nodes,Elements,Types,Forces(:,:,1))
title('Shear forces along the y-axis')

figure
plotforc('shearz',Nodes,Elements,Types,Forces(:,:,1))
title('Shear forces along the z-axis')

figure
plotforc('momx',Nodes,Elements,Types,Forces(:,:,1))
title('Torsional moments')

figure
plotforc('momy',Nodes,Elements,Types,Forces(:,:,1))
title('Bending moments around the y-axis')

figure
plotforc('momz',Nodes,Elements,Types,Forces(:,:,1))
title('Bending moments around the z-axis')

figure
plotforc('norm',Nodes,Elements,Types,Forces(:,:,2))
title('Normal forces')

figure
plotforc('sheary',Nodes,Elements,Types,Forces(:,:,2))
title('Shear forces along the y-axis')

figure
plotforc('shearz',Nodes,Elements,Types,Forces(:,:,2))
title('Shear forces along the z-axis')

figure
plotforc('momx',Nodes,Elements,Types,Forces(:,:,2))
title('Torsional moments')

figure
plotforc('momy',Nodes,Elements,Types,Forces(:,:,2))
title('Bending moments around the y-axis')

figure
plotforc('momz',Nodes,Elements,Types,Forces(:,:,2))
title('Bending moments around the z-axis')

