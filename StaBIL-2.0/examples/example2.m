% StaBIL manual
% Example 2
% Units: m, kN

% Nodes=[NodID X  Y  Z]
Nodes=  [1     0     0     0;
         2     0.5   0     0;
         3     1     0     0;
         4     0    -0.25  0;
         5     1    -0.25  0;
         6     1.25  0     0;
         7     0.3   0.4   0];          % reference node

% Check the node coordinates as follows:     
figure
plotnodes(Nodes);

% Element types -> {EltTypID EltName}
Types=  {1        'beam';
         2        'truss'};

b=0.02;
h=0.05;
A=0.0002;

% Sections=[SecID A      ky   kz   Ixx Iyy Izz]
Sections=  [1     b*h    Inf  Inf  0   0   b*h^3/12;
            2     A      NaN  NaN  NaN NaN NaN];

% Materials=[MatID E nu];
Materials=  [1     210e6 0.3];              % steel

% Elements=[EltID TypID SecID MatID n1 n2 n3]
Elements=  [1     1     1     1     1  2  7;              
            2     1     1     1     2  3  7;
            3     2     2     1     1  4  NaN;
            4     2     2     1     3  5  NaN;
            5     2     2     1     3  6  NaN];

% Check element definitions as follows:
hold('on');
plotelem(Nodes,Elements,Types);
title('Nodes and elements');

% Degrees of freedom 
% Assemble a column matrix containing all DOFs at which stiffness is
% present in the model:
DOF=getdof(Elements,Types);

% Remove all DOFs equal to zero from the vector:
%  - 2D analysis: select only UX,UY,ROTZ
%  - clamp nodes 4, 5, 6
seldof=[0.03; 0.04; 0.05; 4.00; 5.00; 6.00];
DOF=removedof(DOF,seldof);

% Assembly of stiffness matrix K 
K=asmkm(Nodes,Elements,Types,Sections,Materials,DOF);

% Distributed loads are specified in the global coordinate system
% DLoads=[EltID n1globalX n1globalY n1globalZ ...]
DLoads=  [1     0  0    0 0 -0.02 0;
          2     0 -0.02 0 0  0    0;];

P=elemloads(DLoads,Nodes,Elements,Types,DOF);

% Solve K * U = P 
U=K\P;

% Plot displacements
figure
plotdisp(Nodes,Elements,Types,DOF,U,DLoads,Sections,Materials)
title('Displacements')

% The displacements can be displayed as follows: 
printdisp(Nodes,DOF,U);

% Compute element forces
Forces=elemforces(Nodes,Elements,Types,Sections,Materials,DOF,U,DLoads);

% The element forces can be displayed in an orderly table:
printforc(Elements,Forces);

% Plot element forces
figure
plotforc('norm',Nodes,Elements,Types,Forces,DLoads)
title('Normal forces')

figure
plotforc('sheary',Nodes,Elements,Types,Forces,DLoads)
title('Shear forces')

figure
plotforc('momz',Nodes,Elements,Types,Forces,DLoads)
title('Bending moments')
