function [Ax,Ay,Az,B,Cx,Cy,Cz]=disp_shell4(Nodes,Elements,DOF,EltIDDLoad,Sections,Materials)

%DISP_SHELL4    Matrices to compute the displacements of the deformed shell4.
%
%   [Ax,Ay,Az,B] = disp_shell4(Nodes,Elements,DOF,U)
%   returns the matrices to compute the displacements of the deformed shell4.
%   The coordinates of the nodes of the shell4 element are 
%   computed using X=Ax*U+B(:,1); Y=Ay*U+B(:,2) and 
%   Z=Az*U+B(:,3). 
%
%   Nodes      Node definitions        [NodID x y z]
%   Elements   Element definitions     [EltID TypID SecID MatID n1 n2 ...]
%   DOF        Degrees of freedom  (nDOF * 1)
%   Ax         Matrix to compute the x-coordinates of the deformations 
%   Ay         Matrix to compute the y-coordinates of the deformations 
%   Az         Matrix to compute the z-coordinates of the deformations 
%   B          Matrix which contains the x-, y- and z-coordinates of the
%              undeformed structure
%
%   See also DISP_TRUSS, PLOTDISP, DISP_SHELL8.

% Miche Jansen
% 2009

DOF=DOF(:);
nElem=size(Elements,1);
nDOF=size(DOF,1);
nPoints=1;

% Ax=sparse([],[],[],nElem*(4*nPoints+2),nDOF);
% Ay=sparse([],[],[],nElem*(4*nPoints+2),nDOF);
% Az=sparse([],[],[],nElem*(4*nPoints+2),nDOF);
B=zeros(nElem*(4*nPoints+2),3);

Ax=sparse([],[],[],nElem*(4*nPoints+2),24*nElem);
Ay=sparse([],[],[],nElem*(4*nPoints+2),24*nElem); 
Az=sparse([],[],[],nElem*(4*nPoints+2),24*nElem);

Nx = zeros(4*nPoints+1,24);
Ny = zeros(4*nPoints+1,24);  
Nz = zeros(4*nPoints+1,24);
    
Nx(1,1) = 1;
Nx(2,7) = 1;
Nx(3,13) = 1;
Nx(4,19) = 1;
Ny(1,2) = 1;
Ny(2,8) = 1;
Ny(3,14) = 1;
Ny(4,20) = 1;
Nz(1,3) = 1;
Nz(2,9) = 1;
Nz(3,15) = 1;
Nz(4,21) = 1;
Nx(5,1) = 1;
Ny(5,2) = 1;
Nz(5,3) = 1;

for iElem = 1:nElem
    
    NodeNum=Elements(iElem,5:end);
    
    Node=zeros(length(NodeNum),3);
    for iNode=1:length(NodeNum)
        loc=find(Nodes(:,1)==NodeNum(1,iNode));
        if isempty(loc)
            Node(iNode,:)=[NaN NaN NaN];
        elseif length(loc)>1
            error('Node %i is multiply defined.',NodeNum(1,iNode))
        else
            Node(iNode,:)=Nodes(loc,2:end);
        end
    end
    
    dofelem=dof_shell4(NodeNum);
    
    C=selectdof(DOF,dofelem);
    
    B((1:(4*nPoints+1))+(iElem-1)*(4*nPoints+2),1) = Node([1 2 3 4 1],1);
    B((1:(4*nPoints+1))+(iElem-1)*(4*nPoints+2),2) = Node([1 2 3 4 1],2);
    B((1:4*nPoints+1)+(iElem-1)*(4*nPoints+2),3) = Node([1 2 3 4 1],3);

    B(iElem*(4*nPoints+2),:) = NaN;
   
    Ca{iElem,1} = C; % met cell-array werken voor snelheid
%     Ax((1:4*nPoints+1)+(iElem-1)*(4*nPoints+2),1:nDOF) = Nx*C;
%     Ay((1:4*nPoints+1)+(iElem-1)*(4*nPoints+2),1:nDOF) = Ny*C;
%     Az((1:4*nPoints+1)+(iElem-1)*(4*nPoints+2),1:nDOF) = Nz*C;

    Ax((1:4*nPoints+1)+(iElem-1)*(4*nPoints+2),24*(iElem-1)+(1:24)) = Nx;
    Ay((1:4*nPoints+1)+(iElem-1)*(4*nPoints+2),24*(iElem-1)+(1:24)) = Ny;
    Az((1:4*nPoints+1)+(iElem-1)*(4*nPoints+2),24*(iElem-1)+(1:24)) = Nz;

end

Ca = cell2mat(Ca);
Ax = Ax*Ca;
Ay = Ay*Ca;
Az = Az*Ca;

if nargout > 4
    Cx = 0;
    Cy = 0; % er wordt geen rekening gehouden met de vervormingen door de verdeelde belastingen
    Cz = 0;
end

end
    
    
