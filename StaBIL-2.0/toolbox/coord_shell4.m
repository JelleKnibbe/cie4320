function [X,Y,Z]=coord_shell4(Nodes,NodeNum)

%COORD_SHELL4  Coordinates of the shell elements for plotting.
%
%   [X,Y,Z] = coord_shell4(Nodes,NodeNum)
%   returns the coordinates of the shell4 elements for plotting.
%
%   Nodes      Node definitions        [NodID x y z] (nNodes * 4)
%   NodeNum    Node numbers            [NodID1 NodID2 NodID3 NodID4] (nElem * 4)
%   X          X coordinates  (4 * nElem)    
%   Y          Y coordinates  (4 * nElem)
%   Z          Z coordinates  (4 * nElem)
%
%   See also COORD_TRUSS, PLOTELEM.

% Miche Jansen
% 2009

nElem=size(NodeNum,1);
nNode = 4;
X=zeros(nNode,nElem);
Y=zeros(nNode,nElem);
Z=zeros(nNode,nElem);

for iElem=1:nElem
    for ind =1:4
    loc=find(Nodes(:,1)==NodeNum(iElem,ind));
    if isempty(loc)
        error('Node %i is not defined.',NodeNum(iElem,1))
    elseif length(loc)>1
        error('Node %i is multiply defined.',NodeNum(iElem,1))
    else
        X(ind,iElem)=Nodes(loc,2);
        Y(ind,iElem)=Nodes(loc,3);
        Z(ind,iElem)=Nodes(loc,4);
    end
    end
end   