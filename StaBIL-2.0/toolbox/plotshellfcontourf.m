function h = plotshellfcontourf(ftype,Nodes,Elements,Types,F,varargin)

%PLOTSHELLFCONTOURF   Plot filled contours of shell forces in shell elements.
%
%    plotshellfcontourf(stype,Nodes,Elements,Types,F)
%    plots force contours (output from ELEMSTRESS).
%
%   ftype      'Nx'       Membrane forces 
%              'Ny'     
%              'Nxy'     
%              'Mx'       Bending moments
%              'My'      
%              'Mxy'
%              'Vx'       Shear forces
%              'Vy'
%   Nodes      Node definitions        [NodID x y z]
%   Elements   Element definitions     [EltID TypID SecID MatID n1 n2 ...]
%   Types      Element type definitions  {TypID EltName Option1 ... }
%   F          Forces/moments per unit length 
%              (nElem * 32)              [Nx Ny Nxy Mx My Mxy Vx Vy]
%   plotshellfcontourf(...,ParamName,ParamValue) sets the value of the
%   specified parameters.  The following parameters can be specified:
%   'GCS'          Plot the GCS. Default: 'on'.
%   'Undeformed'   Plots the undeformed mesh.  Default: 'k-'.
%   'ncolor'       Number of colors in colormap. Default: 10.
%   'Handle'       Plots in the axis with this handle.  Default: current axis.
%
%   See also ELEMSHELLF, PATCH_SHELL8, PATCH_SHELL4.

% Miche Jansen
% 2010


if nargin<6                               
    paramlist={};
elseif nargin>5 && ischar(varargin{1})
    paramlist=varargin;
end

% PARAMETERS
[location,paramlist]=cutparam('location','top',paramlist);
[GCS,paramlist]=cutparam('GCS','on',paramlist);
[Undeformed,paramlist]=cutparam('Undeformed','k-',paramlist);
[ncolor,paramlist]=cutparam('ncolor',11,paramlist);
[haxis,paramlist]=cutparam('Handle',gca,paramlist);



switch lower(ftype)
    case {'nx'}
        ind1 = 0;
    case {'ny'}
        ind1 = 1;
    case {'nxy'}
        ind1 = 2;
    case {'mx'}
        ind1 = 3;
    case {'my'}
        ind1 = 4;
    case {'mxy'}
        ind1 = 5;
    case {'vx'}
        ind1 = 6;
    case {'vy'}
        ind1 = 7;
end

[Fmax,maxi] = max(F(:,(1:8:25)+ind1)); 
[Fmin,mini] = min(F(:,(1:8:25)+ind1));
[Fmax,maxj] = max(Fmax);
[Fmin,minj] = min(Fmin);
maxi = maxi(maxj);
mini = mini(minj);

nType = size(Types,1);
pxyz = [];
pind = [];
pvalues = [];

for iType=1:nType
    TypID=Types{iType,1};
    Type=Types{iType,2};
    ind=find(Elements(:,2)==TypID);
    NodeNumbers=Elements(ind,5:end);
    F2 = F(ind,(1:8:25)+ind1); 
    [pxyz2,pind2,pvalues2]=eval(['patch_' Type '(Nodes,NodeNumbers,F2)']); % aanpassen!!!
    
    if size(pind2,2) < size(pind,2)
        pind2 = [pind2,nan(size(pind2,1),size(pind,2)-size(pind2,2))];
    elseif size(pind,2) < size(pind2,2)
        pind = [pind,nan(size(pind,1),size(pind2,2)-size(pind,2))];
    end
    pind=[pind;pind2+size(pxyz,1)];
    pxyz=[pxyz;pxyz2];
    pvalues=[pvalues;pvalues2];
end


% PREPARE FIGURE
haxis=newplot(haxis);
nextplot=get(haxis,'NextPlot');


% PLOT CONTOUR
h.elem=patch('Vertices',pxyz,'Faces',pind,'FaceVertexCData',pvalues,'FaceColor','interp');
colormap(jet(ncolor));
set(gcf,'renderer','painters')

% MIN & MAX
loc = find(Nodes(:,1)==Elements(maxi(1),4+maxj(1)));
xyzmax = Nodes(loc(1),2:4);
loc = find(Nodes(:,1)==Elements(mini(1),4+minj(1)));
xyzmin = Nodes(loc(1),2:4);
set(haxis,'NextPlot','add');
h.minmax=text([xyzmax(1);xyzmin(1)],[xyzmax(2);xyzmin(2)],[xyzmax(3);xyzmin(3)],{'MAX';'MIN'});

% COLORBAR
v = caxis;
yticks = linspace(v(1),v(2),ncolor+1);
h.cbar=colorbar('peer',haxis,'YTickMode','auto','location','westoutside','YTickLabelMode','auto','YTick',yticks,'YLim',v);



% VIEWPOINT AND AXES FIGURE
if strcmpi(nextplot,'replace')
    if all(Nodes(:,4)==0)
        view(0,90);
    elseif all(Nodes(:,3)==0)
        view(0,0);
    elseif all(Nodes(:,2)==0)
        view(90,0);
    else
        view(37.5,30);
    end
    defaultposition=[0.13 0.11 0.775 0.815];
    if all(get(haxis,'Position')==defaultposition)
        set(haxis,'Position',[0.05 0.05 0.9 0.87])
    end
    axis equal
    set(gcf,'resizefcn',@figResize)
    axis fill
    axis off
    set(gcf,'PaperPositionMode','auto')
end

% PLOT GLOBAL COORDINATE SYSTEM
if strcmpi(GCS,'on')
    lref=reflength(Nodes);
    set(haxis,'NextPlot','add');
    h=plotgcs(lref,h);
end

% RESET NEXTPLOT STATE
set(haxis,'NextPlot',nextplot);

% RETURN OUTPUT ARGUMENTS ONLY IF REQUESTED
if nargout<1, clear('h'); end

%-------------------------------------------------------------------------------

% CUT PARAMETER FROM LIST
function [value,paramlist]=cutparam(name,default,paramlist);
value=default;
for iarg=length(paramlist)-1:-1:1
    if strcmpi(name,paramlist{iarg})
        value=paramlist{iarg+1};
        paramlist=paramlist([1:iarg-1 iarg+2:end]);
        break
    end
end
