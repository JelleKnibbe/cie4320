function [phi,omega]=eigfem(K,M,nMode)

%EIGFEM  Compute the eigenmodes and eigenfrequencies of the finite element model.
%
%   [phi,omega]=eigfem(K,M,nMode)
%   [phi,omega]=eigfem(K,M)
%   computes the eigenmodes and eigenfrequencies of the finite element model.
%
%   K          Stiffness matrix (nDOF * nDOF)
%   M          Mass matrix (nDOF * nDOF)
%   nMode      Number of eigenmodes and eigenfrequencies (default: all)
%   phi        Eigenmodes (in columns) (nDOF * nMode)
%   omega      Eigenfrequencies [rad/s] (nMode * 1)

% David Dooms
% March 2008

if (nargin < 3) || (nMode==length(K))
    [phi,Lambda]=eig(full(K),full(M));
    omega=sqrt(diag(Lambda));
    [omega,index]=sort(omega);
    phi=phi(:,index);
    n=sqrt(diag(phi.'*M*phi));                          % normalize
    phi=phi./repmat(n.',size(phi,1),1);   
else
%     try                                                     % K and M symmetric
%         OPTS.disp=0;
%         [phi,Lambda] = eigs(K,M,nMode,'sm',OPTS);
%         [omega,index]=sort(sqrt(diag(Lambda)));
%         phi=phi(:,index);
%     catch                                                   % K and M not symmetric
        [phi,Lambda]=eig(full(K),full(M));
        omega=sqrt(diag(Lambda));
        [omega,index]=sort(omega);
        phi=phi(:,index);
        if nargin > 2
            omega=omega(1:nMode);
            phi=phi(:,1:nMode); 
        end
        n=sqrt(diag(phi.'*M*phi));                          % normalize
        phi=phi./repmat(n.',size(phi,1),1); 
%    end 
end

