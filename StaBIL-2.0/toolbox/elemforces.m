function [ForcesLCS,ForcesGCS]=elemforces(Nodes,Elements,Types,Sections,Materials,DOF,U,DLoads)

%ELEMFORCES   Compute the element forces.
%
%   [ForcesLCS,ForcesGCS]
%              =elemforces(Nodes,Elements,Types,Sections,Materials,DOF,U,DLoads)
%   [ForcesLCS,ForcesGCS]
%              =elemforces(Nodes,Elements,Types,Sections,Materials,DOF,U)
%   computes the element forces in the local (beam convention) and the  
%   global (algebraic convention) coordinate system.
%
%   Nodes      Node definitions          [NodID x y z]
%   Elements   Element definitions       [EltID TypID SecID MatID n1 n2 ...]
%   Types      Element type definitions  {TypID EltName Option1 ... }
%   Sections   Section definitions       [SecID SecProp1 SecProp2 ...]
%   Materials  Material definitions      [MatID MatProp1 MatProp2 ... ]
%   DOF        Degrees of freedom  (nDOF * 1)
%   U          Displacements (nDOF * 1)
%   DLoads     Distributed loads       [EltID n1globalX n1globalY n1globalZ ...]
%   ForcesLCS  Element forces in LCS (beam convention) [N Vy Vz T My Mz] 
%                                                                   (nElem * 12)
%   ForcesGCS  Element forces in GCS (algebraic convention) (nElem * 12)
%
%   See also FORCES_TRUSS, FORCES_BEAM.

% David Dooms
% October 2008

nElem=size(Elements,1);
nTimeSteps=size(U,2);

ForcesLCS=zeros(nElem,12,nTimeSteps);
ForcesGCS=zeros(nElem,12,nTimeSteps);

for iElem=1:nElem

    TypID=Elements(iElem,2);
    loc=find(cell2mat(Types(:,1))==TypID);
    if isempty(loc)
        error('Element type %i is not defined.',TypID)
    elseif length(loc)>1
        error('Element type %i is multiply defined.',TypID)
    end
    
    Type=Types{loc,2};
    
    if size(Types,2)<3
        Options={};
    else
        Options=Types{loc,3};
    end
    
    SecID=Elements(iElem,3);
    loc=find(Sections(:,1)==SecID);
    if isempty(loc)
        error('Section %i is not defined.',SecID)
    elseif length(loc)>1
        error('Section %i is multiply defined.',SecID)
    end
    Section=Sections(loc,2:end);
    
    MatID=Elements(iElem,4);
    loc=find(Materials(:,1)==MatID);
    if isempty(loc)
        error('Material %i is not defined.',MatID)
    elseif length(loc)>1
        error('Material %i is multiply defined.',MatID)
    end
    Material=Materials(loc,2:end);
    
    NodeNum=Elements(iElem,5:end);
    
    Node=zeros(length(NodeNum),3);
    for iNode=1:length(NodeNum)
        loc=find(Nodes(:,1)==NodeNum(1,iNode));
        if isempty(loc)
            Node(iNode,:)=[NaN NaN NaN];
        elseif length(loc)>1
            error('Node %i is multiply defined.',NodeNum(1,iNode))
        else
            Node(iNode,:)=Nodes(loc,2:end);
        end
    end
    
    UeGCS=elemdisp(Type,NodeNum,DOF,U);
    
    EltID=Elements(iElem,1);
    
    if nargin<8
        DLoads=zeros(1,7,nTimeSteps);
    end
    loc=find(DLoads(:,1)==EltID);
    if isempty(loc)
        DLoad=zeros(6,nTimeSteps);
    elseif length(loc)>1
        error('Element %i has multiple distributed loads.',EltID)
    else
        DLoad=permute(DLoads(loc,2:end,:),[2 3 1]);
    end
    
    [temp1,temp2]=eval(['forces_' Type '(Node,Section,Material,UeGCS,DLoad,Options)']);
    
    ForcesLCS(iElem,:,:) = permute(temp1,[3 1 2]);
    ForcesGCS(iElem,:,:) = permute(temp2,[3 1 2]);

end

% change to beam convention
ForcesLCS(:,1:5,:)=-ForcesLCS(:,1:5,:);
ForcesLCS(:,12,:)=-ForcesLCS(:,12,:);
