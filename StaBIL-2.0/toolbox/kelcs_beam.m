function [KeLCS,MeLCS] = kelcs_beam(L,A,ky,kz,Ixx,Iyy,Izz,E,nu,rho,Options)

%KELCS_BEAM   Beam element stiffness and mass matrix in local coordinate system.
%
%   [KeLCS,MeLCS] = kelcs_beam(L,A,ky,kz,Ixx,Iyy,Izz,E,nu,rho,Options) 
%   [KeLCS,MeLCS] = kelcs_beam(L,A,ky,kz,Ixx,Iyy,Izz,E,nu,rho) 
%    KeLCS    = kelcs_beam(L,A,ky,kz,Ixx,Iyy,Izz,E,nu)
%   returns the element stiffness and mass matrix in the local coordinate system 
%   for a two node beam element (isotropic material)
%
%   L          Beam length 
%   A          Beam cross section
%   ky         Shear deflection factor A_sy = ky * A
%   kz         Shear deflection factor A_sz = kz * A
%   Ixx        Moment of inertia
%   Iyy        Moment of inertia
%   Izz        Moment of inertia
%   E          Young's modulus
%   nu         Poisson coefficient
%   rho        Mass density
%   Options    Options for the mass matrix: {'lumped'}, {'norotaroryinertia'}
%   KeLCS      Element stiffness matrix (12 * 12)
%   MeLCS      Element mass matrix (12 * 12)
%
%   See also KE_BEAM, KELCS_TRUSS.

% David Dooms
% March 2008

%%% CONTROLES %%% alles groter dan nul, nu kleiner dan 0.5
% bij ky en kz wordt heel kleine waarde opgeteld om problemen te vermijden als ze gelijk zijn aan nul
% default for Ixx?
% added mass?

%% STIFFNESS MATRIX %%
% Przemieniecki, J. S., Theory of Matrix Structural Analysis, McGraw-Hill,
% New York (1968).

KeLCS=sparse([],[],[],12,12,40);

G=E/(2*(1+nu));

phi_y=(12*E*Izz)/(G*A*(ky+1e-250)*L^2);
phi_z=(12*E*Iyy)/(G*A*(kz+1e-250)*L^2);

% terms with A
KeLCS(1,1)=A*E/L;
KeLCS(7,7)=KeLCS(1,1);
KeLCS(7,1)=-KeLCS(1,1);

% terms with Ixx
KeLCS(4,4)=Ixx*G/L;
KeLCS(10,10)=KeLCS(4,4);
KeLCS(10,4)=-KeLCS(4,4);
                     
% terms with Izz
KeLCS(2,2)=(12*E*Izz)/(L^3*(1+phi_y));
KeLCS(8,8)=KeLCS(2,2);
KeLCS(8,2)=-KeLCS(2,2);

KeLCS(6,2)=(6*E*Izz)/(L^2*(1+phi_y));
KeLCS(12,2)=KeLCS(6,2);
KeLCS(8,6)=-KeLCS(6,2);
KeLCS(12,8)=-KeLCS(6,2);

KeLCS(6,6)=E*Izz/L*(4+phi_y)/(1+phi_y);
KeLCS(12,12)=KeLCS(6,6);

KeLCS(12,6)=E*Izz/L*(2-phi_y)/(1+phi_y);

% terms with Iyy
KeLCS(3,3)=(12*E*Iyy)/(L^3*(1+phi_z));
KeLCS(9,9)=KeLCS(3,3);
KeLCS(9,3)=-KeLCS(3,3);

KeLCS(9,5)=(6*E*Iyy)/(L^2*(1+phi_z));
KeLCS(11,9)=KeLCS(9,5);
KeLCS(5,3)=-KeLCS(9,5);
KeLCS(11,3)=-KeLCS(9,5);

KeLCS(5,5)=E*Iyy/L*(4+phi_z)/(1+phi_z);
KeLCS(11,11)=KeLCS(5,5);

KeLCS(11,5)=E*Iyy/L*(2-phi_z)/(1+phi_z);

% symmetry
KeLCS(1,7)=KeLCS(7,1);
KeLCS(4,10)=KeLCS(10,4);
KeLCS(2,8)=KeLCS(8,2);
KeLCS(2,6)=KeLCS(6,2);
KeLCS(2,12)=KeLCS(12,2);
KeLCS(6,8)=KeLCS(8,6);
KeLCS(8,12)=KeLCS(12,8);
KeLCS(6,12)=KeLCS(12,6);
KeLCS(3,9)=KeLCS(9,3);
KeLCS(5,9)=KeLCS(9,5);
KeLCS(9,11)=KeLCS(11,9);
KeLCS(3,5)=KeLCS(5,3);
KeLCS(3,11)=KeLCS(11,3);
KeLCS(5,11)=KeLCS(11,5);

%% MASS MATRIX %%

if nargout>1 

if nargin<11
    Options={};
end

if ~ any(strcmpi(Options,'lumped'))
    
%consistent

MeLCS=sparse([],[],[],12,12,40);

% Yokoyama, T., "Vibrations of a Hanging Timoshenko Beam Under Gravity", Journal
% of Sound and Vibration, Vol. 141, No. 2, pp. 245-258 (1990).

if any(strcmpi(Options,'norotatoryinertia'))
    ry=0;
    rz=0;
else
    ry=sqrt(Iyy/A);
    rz=sqrt(Izz/A);
end

% axial terms 
MeLCS(1,1)=rho*A*L/3;
MeLCS(7,7)=MeLCS(1,1);
MeLCS(7,1)=rho*A*L/6;

% torsional terms
MeLCS(4,4)=rho*L*(Iyy+Izz)/3;
MeLCS(10,10)=MeLCS(4,4);
MeLCS(10,4)=rho*L*(Iyy+Izz)/6;

% terms with Izz
MeLCS(2,2)=rho*A*L/(1+phi_y)^2*(13/35+7/10*phi_y+1/3*phi_y^2+6/5*(rz/L)^2);
MeLCS(8,8)=MeLCS(2,2);
MeLCS(8,2)=rho*A*L/(1+phi_y)^2*(9/70+3/10*phi_y+1/6*phi_y^2-6/5*(rz/L)^2);

MeLCS(6,2)=rho*A*L/(1+phi_y)^2*(11/210+11/120*phi_y+1/24*phi_y^2+(1/10-1/2*phi_y)*(rz/L)^2)*L;
MeLCS(12,8)=-MeLCS(6,2); %controleren!!!!!
MeLCS(8,6)=rho*A*L/(1+phi_y)^2*(13/420+3/40*phi_y+1/24*phi_y^2-(1/10-1/2*phi_y)*(rz/L)^2)*L;
MeLCS(12,2)=-MeLCS(8,6);

MeLCS(6,6)=rho*A*L/(1+phi_y)^2*(1/105+1/60*phi_y+1/120*phi_y^2+(2/15+1/6*phi_y+1/3*phi_y^2)*(rz/L)^2)*L^2;
MeLCS(12,12)=MeLCS(6,6);

MeLCS(12,6)=-rho*A*L/(1+phi_y)^2*(1/140+1/60*phi_y+1/120*phi_y^2+(1/30+1/6*phi_y-1/6*phi_y^2)*(rz/L)^2)*L^2;

% terms with Iyy
MeLCS(3,3)=rho*A*L/(1+phi_z)^2*(13/35+7/10*phi_z+1/3*phi_z^2+6/5*(ry/L)^2);
MeLCS(9,9)=MeLCS(3,3);
MeLCS(9,3)=rho*A*L/(1+phi_z)^2*(9/70+3/10*phi_z+1/6*phi_z^2-6/5*(ry/L)^2);

MeLCS(5,3)=-rho*A*L/(1+phi_z)^2*(11/210+11/120*phi_z+1/24*phi_z^2+(1/10-1/2*phi_z)*(ry/L)^2)*L;
MeLCS(11,9)=-MeLCS(5,3); %controleren!!!!!
MeLCS(9,5)=-rho*A*L/(1+phi_z)^2*(13/420+3/40*phi_z+1/24*phi_z^2-(1/10-1/2*phi_z)*(ry/L)^2)*L;
MeLCS(11,3)=-MeLCS(9,5);

MeLCS(5,5)=rho*A*L/(1+phi_z)^2*(1/105+1/60*phi_z+1/120*phi_z^2+(2/15+1/6*phi_z+1/3*phi_z^2)*(ry/L)^2)*L^2;
MeLCS(11,11)=MeLCS(5,5);

MeLCS(11,5)=-rho*A*L/(1+phi_z)^2*(1/140+1/60*phi_z+1/120*phi_z^2+(1/30+1/6*phi_z-1/6*phi_z^2)*(ry/L)^2)*L^2;

% symmetry
MeLCS(1,7)=MeLCS(7,1);
MeLCS(4,10)=MeLCS(10,4);
MeLCS(2,8)=MeLCS(8,2);
MeLCS(2,6)=MeLCS(6,2);
MeLCS(2,12)=MeLCS(12,2);
MeLCS(6,8)=MeLCS(8,6);
MeLCS(8,12)=MeLCS(12,8);
MeLCS(6,12)=MeLCS(12,6);
MeLCS(3,9)=MeLCS(9,3);
MeLCS(5,9)=MeLCS(9,5);
MeLCS(9,11)=MeLCS(11,9);
MeLCS(3,5)=MeLCS(5,3);
MeLCS(3,11)=MeLCS(11,3);
MeLCS(5,11)=MeLCS(11,5);

else   % lumped 

MeLCS=sparse([],[],[],12,12,12);

MeLCS(1,1)=rho*A*L/2;
MeLCS(2,2)=MeLCS(1,1);
MeLCS(3,3)=MeLCS(1,1);
MeLCS(7,7)=MeLCS(1,1);
MeLCS(8,8)=MeLCS(1,1);
MeLCS(9,9)=MeLCS(1,1);

MeLCS(4,4)=rho*Ixx*L/2; % controleren!!!!!
MeLCS(10,10)=MeLCS(4,4);

MeLCS(5,5)=rho*A*L^3/24; % controleren!!!!! = rho*L/2*(Iyy/A+L^2/12); p594 Wunderlich
MeLCS(6,6)=MeLCS(5,5);
MeLCS(11,11)=MeLCS(5,5);
MeLCS(12,12)=MeLCS(5,5);

end

end

