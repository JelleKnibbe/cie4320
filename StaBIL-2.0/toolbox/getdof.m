function [DOF,elemInd] = getdof(Elements,Types)

%GETDOF   Get the vector with the degrees of freedom of the model.
%
%   DOF=getdof(Elements,Types) builds the vector with the 
%   labels of the degrees of freedom for which stiffness is present in the 
%   finite element model.
%
%   Elements   Element definitions       [EltID TypID SecID MatID n1 n2 ...]
%   Types      Element type definitions  {TypID EltName Option1 ... }
%   DOF        Degrees of freedom  (nDOF * 1)
%
%   See also DOF_TRUSS, DOF_BEAM, GETDOF.



nElem=size(Elements,1);

nterm=0;

for iElem=1:nElem

    TypID=Elements(iElem,2);
    loc=find(cell2mat(Types(:,1))==TypID);
    if isempty(loc)
        error('Element type %i is not defined.',TypID)
    elseif length(loc)>1
        error('Element type %i is multiply defined.',TypID)
    end
    
    Type=Types{loc,2};
        
    NodeNum=Elements(iElem,5:end);
    
    dofelem=eval(['dof_' Type '(NodeNum)']);
    
    elemInd{iElem}=nterm+(1:length(dofelem));
    DOF(nterm+(1:length(dofelem)),1)=dofelem;
    
    nterm=nterm+length(dofelem);
    
end

[DOF,iDOF,jDOF]=unique(DOF);

for iElem=1:nElem
elemInd{iElem}=jDOF(elemInd{iElem});
end


