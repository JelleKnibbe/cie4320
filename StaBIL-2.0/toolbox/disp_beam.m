function [Ax,Ay,Az,B,Cx,Cy,Cz]=disp_beam(Nodes,Elements,DOF,EltIDDLoad,Sections,Materials,Points)

%DISP_BEAM   Return matrices to compute the displacements of the deformed beams.
%
%   [Ax,Ay,Az,B,Cx,Cy,Cz]
%            =disp_beam(Nodes,Elements,DOF,EltIDDLoad,Sections,Materials,Points)
%   [Ax,Ay,Az,B,Cx,Cy,Cz]
%            =disp_beam(Nodes,Elements,DOF,EltIDDLoad,Sections,Materials)
%   [Ax,Ay,Az,B]
%            =disp_beam(Nodes,Elements,DOF)
%   returns the matrices to compute the displacements of the deformed beams.
%   The coordinates of the specified points along the deformed beams element are 
%   computed using X=Ax*U+Cx*DLoad+B(:,1); Y=Ay*U+Cy*DLoad+B(:,2) and 
%   Z=Az*U+Cz*DLoad+B(:,3). The matrices Cx,Cy and Cz superimpose the 
%   displacements that occur due to the distributed loads if all nodes are fixed.
%
%   Nodes      Node definitions        [NodID x y z]
%   Elements   Element definitions     [EltID TypID SecID MatID n1 n2 ...]
%   DOF        Degrees of freedom  (nDOF * 1)
%   EltIDDLoad Elements with distributed loads [EltID]
%   Sections   Section definitions       [SecID SecProp1 SecProp2 ...]
%   Materials  Material definitions      [MatID MatProp1 MatProp2 ... ]
%   Points     Points in the local coordinate system (1 * nPoints)
%   Ax         Matrix to compute the x-coordinates of the deformations 
%   Ay         Matrix to compute the y-coordinates of the deformations 
%   Az         Matrix to compute the z-coordinates of the deformations 
%   B          Matrix which contains the x-, y- and z-coordinates of the 
%              undeformed structure
%   Cx         Matrix to compute the x-coordinates of the deformations 
%   Cy         Matrix to compute the y-coordinates of the deformations 
%   Cz         Matrix to compute the z-coordinates of the deformations 
%
%   See also DISP_TRUSS, PLOTDISP, NELCS_BEAM, NEDLOADLCS_BEAM.

% David Dooms
% September 2008

% PREPROCESSING
DOF=DOF(:);

if nargin<7
    nPoints=21;
    Points=linspace(0,1,nPoints);
else
    nPoints=length(Points);
end

nElem=size(Elements,1);
nDOF=size(DOF,1);
NeLCS=nelcs_beam(Points);
Ax=sparse([],[],[],nElem*(nPoints+1),nDOF,nElem*nPoints*12+nElem);
Ay=sparse([],[],[],nElem*(nPoints+1),nDOF,nElem*nPoints*12+nElem);
Az=sparse([],[],[],nElem*(nPoints+1),nDOF,nElem*nPoints*12+nElem);
B=zeros(nElem*(nPoints+1),3);

if nargin>3          % en als DLoad leeg is?  materiaal en sectie ontbreken?
    EltIDDLoad=EltIDDLoad(:);
    nDLoad=size(EltIDDLoad,1);
    NeDLoad=nedloadlcs_beam(Points);
    Cx=sparse([],[],[],nElem*(nPoints+1),nDLoad*6,nDLoad*nPoints*6);
    Cy=sparse([],[],[],nElem*(nPoints+1),nDLoad*6,nDLoad*nPoints*6);
    Cz=sparse([],[],[],nElem*(nPoints+1),nDLoad*6,nDLoad*nPoints*6);
end

for iElem=1:nElem
    
    NodeNum=Elements(iElem,5:end);
    
    Node=zeros(length(NodeNum),3);
    for iNode=1:length(NodeNum)
        loc=find(Nodes(:,1)==NodeNum(1,iNode));
        if isempty(loc)
            Node(iNode,:)=[NaN NaN NaN];
        elseif length(loc)>1
            error('Node %i is multiply defined.',NodeNum(1,iNode))
        else
            Node(iNode,:)=Nodes(loc,2:end);
        end
    end
    
    dofelem=dof_beam(NodeNum);
    
    C=selectdof(DOF,dofelem);
    
    % transform displacements from global to local coordinate system
    t=trans_beam(Node);
    T=blkdiag(t,t,t,t);
    
    % compute element length
    L=norm(Node(2,:)-Node(1,:));
    
    tempx=NeLCS*diag([1 0 0 0 0 0 1 0 0 0 0 0])*T;
    tempy=NeLCS*diag([0 1 0 0 0 L 0 1 0 0 0 L])*T;
    tempz=NeLCS*diag([0 0 1 0 L 0 0 0 1 0 L 0])*T;
    
    Ax(((nPoints+1)*(iElem-1)+1):((nPoints+1)*iElem-1),1:nDOF)=(t(1,1)*tempx+t(2,1)*tempy+t(3,1)*tempz)*C;
    Ay(((nPoints+1)*(iElem-1)+1):((nPoints+1)*iElem-1),1:nDOF)=(t(1,2)*tempx+t(2,2)*tempy+t(3,2)*tempz)*C;
    Az(((nPoints+1)*(iElem-1)+1):((nPoints+1)*iElem-1),1:nDOF)=(t(1,3)*tempx+t(2,3)*tempy+t(3,3)*tempz)*C;
    
    B(((nPoints+1)*(iElem-1)+1):((nPoints+1)*iElem-1),1:3)=[NeLCS(:,1), NeLCS(:,7)]*Node(1:2,:);
    
    B((nPoints+1)*iElem,1)=NaN;
    
    if nargin>3          % en als DLoad leeg is?  materiaal en sectie ontbreken?
        EltID=Elements(iElem,1);
        
        iDLoad=find(EltIDDLoad==EltID);
        if length(iDLoad)>1
            error('Element %i has multiple distributed loads.',EltID)
        elseif length(iDLoad)==1
            SecID=Elements(iElem,3);
            loc=find(Sections(:,1)==SecID);
            if isempty(loc)
                error('Section %i is not defined.',SecID)
            elseif length(loc)>1
                error('Section %i is multiply defined.',SecID)
            end
            Iyy=Sections(loc,6);
            Izz=Sections(loc,7);
            
            MatID=Elements(iElem,4);
            loc=find(Materials(:,1)==MatID);
            if isempty(loc)
                error('Material %i is not defined.',MatID)
            elseif length(loc)>1
                error('Material %i is multiply defined.',MatID)
            end
            E=Materials(loc,2);
            
            % transform distributed loads from global to local coordinate system
            T=blkdiag(t,t);
            
            tempx=L^4/(120*E)*NeDLoad*diag([0 0 0 0 0 0])*T;
            tempy=L^4/(120*E)*NeDLoad*diag([0 1/(Izz+1e-250) 0 0 1/(Izz+1e-250) 0])*T;
            tempz=L^4/(120*E)*NeDLoad*diag([0 0 1/(Iyy+1e-250) 0 0 1/(Iyy+1e-250)])*T;
            
            Cx(((nPoints+1)*(iElem-1)+1):((nPoints+1)*iElem-1),((iDLoad-1)*6+1):(iDLoad*6))=t(1,1)*tempx+t(2,1)*tempy+t(3,1)*tempz;
            Cy(((nPoints+1)*(iElem-1)+1):((nPoints+1)*iElem-1),((iDLoad-1)*6+1):(iDLoad*6))=t(1,2)*tempx+t(2,2)*tempy+t(3,2)*tempz;
            Cz(((nPoints+1)*(iElem-1)+1):((nPoints+1)*iElem-1),((iDLoad-1)*6+1):(iDLoad*6))=t(1,3)*tempx+t(2,3)*tempy+t(3,3)*tempz;
        end 
    end
end
