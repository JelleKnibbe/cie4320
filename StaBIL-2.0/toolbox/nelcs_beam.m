function NeLCS = nelcs_beam(Points)

%NELCS_BEAM   Shape functions for a beam element.
%
%   NeLCS = nelcs_beam(Points) determines the values of the shape functions in 
%   the specified points.
%
%   Points     Points in the local coordinate system (1 * nPoints)
%   NeLCS      Values (nPoints * 12)
%
%   See also DISP_BEAM.

% David Dooms
% March 2008

Points=Points(:).';

A=[ 0  0 -1  1;
    2 -3  0  1;
    2 -3  0  1;
    0  0 -1  1;
   -1  2 -1  0;
    1 -2  1  0;
    0  0  1  0;
   -2  3  0  0;
   -2  3  0  0;
    0  0  1  0;
   -1  1  0  0;
    1 -1  0  0;];

NeLCS=zeros(size(Points,2),12);

for k=1:12
    NeLCS(:,k)=polyval(A(k,:),Points);
end
