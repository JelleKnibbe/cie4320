function dof = dof_beam(NodeNum)

%DOF_BEAM   Element degrees of freedom for a beam element.
%
%   dof = dof_beam(NodeNum) builds the vector with the 
%   labels of the degrees of freedom for which stiffness is present in the 
%   beam element.
%
%   NodeNum Node definitions           [NodID1 NodID2] (1 * 2)
%   dof         Degrees of freedom  (12 * 1)
%
%   See also GETDOF.

% David Dooms
% March 2008

dof=zeros(12,1);
dof(1:6,1)=NodeNum(1)+[0.01:0.01:0.06]';
dof(7:12,1)=NodeNum(2)+[0.01:0.01:0.06]';
