function h = plotstresscontourf(stype,Nodes,Elements,Types,S,varargin)

%PLOTSTRESSCONTOURF  Plot filled contours of stress in shell elements.
%
%    plotstresscontourf(stype,Nodes,Elements,Types,S)
%    plots stress contours (output from ELEMSTRESS).
%
%   stype      'sx'       Normal stress (in the global/local x-direction)
%              'sy'     
%              'sz'     
%              'sxy'      Shear stress 
%              'syz'      
%              'sxz'      
%   Nodes      Node definitions        [NodID x y z]
%   Elements   Element definitions     [EltID TypID SecID MatID n1 n2 ...]
%   Types      Element type definitions  {TypID EltName Option1 ... }
%   S          Element stresses in GCS/LCS  [sxx syy szz sxy syz sxz] 
%                                                              (nElem * 72)
%   plotstresscontourf(...,ParamName,ParamValue) sets the value of the 
%   specified parameters.  The following parameters can be specified:
%   'location'     Location (top,mid,bot). Default: 'top'.
%   'GCS'          Plot the GCS. Default: 'on'.
%   'Undeformed'   Plots the undeformed mesh.  Default: 'k-'.
%   'ncolor'       Number of colors in colormap. Default: 10.
%   'Handle'       Plots in the axis with this handle.  Default: current axis.
%
%   See also ELEMSTRESS.

% Miche Jansen
% 2010

if nargin<6                               
    paramlist={};
elseif nargin>5 && ischar(varargin{1})
    paramlist=varargin;
end

% PARAMETERS
[location,paramlist]=cutparam('location','top',paramlist);
[GCS,paramlist]=cutparam('GCS','on',paramlist);
[Undeformed,paramlist]=cutparam('Undeformed','k-',paramlist);
[ncolor,paramlist]=cutparam('ncolor',11,paramlist);
[haxis,paramlist]=cutparam('Handle',gca,paramlist);



switch lower(stype)
    case {'sx','s3'}
        ind1 = 0;
    case {'sy','s2'}
        ind1 = 1;
    case {'sz','s1'}
        ind1 = 2;
    case {'sxy'}
        ind1 = 3;
    case {'syz'}
        ind1 = 4;
    case {'sxz'}
        ind1 = 5;
end

switch lower(location)
    case {'top'}
        ind2 = 0;
    case {'mid'}
        ind2 = 1;
    case {'bot'};
        ind2 = 2;
end

[Smax,maxi] = max(S(:,(1:6:19)+ind1+24*ind2)); 
[Smin,mini] = min(S(:,(1:6:19)+ind1+24*ind2));
[Smax,maxj] = max(Smax);
[Smin,minj] = min(Smin);
maxi = maxi(maxj);
mini = mini(minj);

nType = size(Types,1);
pxyz = [];
pind = [];
pvalues = [];

for iType=1:nType
    TypID=Types{iType,1};
    Type=Types{iType,2};
    ind=find(Elements(:,2)==TypID);
    NodeNumbers=Elements(ind,5:end);
    S2 = S(ind,(1:6:19)+ind1+24*ind2); 
    [pxyz2,pind2,pvalues2]=eval(['patch_' Type '(Nodes,NodeNumbers,S2)']); % aanpassen!!!
    
    if size(pind2,2) < size(pind,2)
        pind2 = [pind2,nan(size(pind2,1),size(pind,2)-size(pind2,2))];
    elseif size(pind,2) < size(pind2,2)
        pind = [pind,nan(size(pind,1),size(pind2,2)-size(pind,2))];
    end
    pind=[pind;pind2+size(pxyz,1)];
    pxyz=[pxyz;pxyz2];
    pvalues=[pvalues;pvalues2];
end


% PREPARE FIGURE
haxis=newplot(haxis);
nextplot=get(haxis,'NextPlot');

% PLOT CONTOUR
h.elem=patch('Vertices',pxyz,'Faces',pind,'FaceVertexCData',pvalues,'FaceColor','interp');
colormap(jet(ncolor));
set(gcf,'renderer','painters')

% MIN & MAX
loc = find(Nodes(:,1)==Elements(maxi(1),4+maxj(1)));
xyzmax = Nodes(loc(1),2:4);
loc = find(Nodes(:,1)==Elements(mini(1),4+minj(1)));
xyzmin = Nodes(loc(1),2:4);
set(haxis,'NextPlot','add');
h.minmax=text([xyzmax(1);xyzmin(1)],[xyzmax(2);xyzmin(2)],[xyzmax(3);xyzmin(3)],{'MAX';'MIN'});

% COLORBAR
v = caxis;
yticks = linspace(v(1),v(2),ncolor+1);
%h.cbar=colorbar('peer',haxis,'YTickMode','manual','location','westoutside','YTickLabelMode','manual','YTick',yticks,'YTickLabel',yticks);
h.cbar=colorbar('peer',haxis,'YTickMode','auto','location','westoutside','YTickLabelMode','auto','YTick',yticks,'YLim',v);


% VIEWPOINT AND AXES FIGURE
if strcmpi(nextplot,'replace')
    if all(Nodes(:,4)==0)
        view(0,90);
    elseif all(Nodes(:,3)==0)
        view(0,0);
    elseif all(Nodes(:,2)==0)
        view(90,0);
    else
        view(37.5,30);
    end
    defaultposition=[0.13 0.11 0.775 0.815];
    if all(get(haxis,'Position')==defaultposition)
        set(haxis,'Position',[0.05 0.05 0.9 0.87])
    end
    axis equal
    set(gcf,'resizefcn',@figResize)
    axis fill
    axis off
    set(gcf,'PaperPositionMode','auto')
end

% PLOT GLOBAL COORDINATE SYSTEM
if strcmpi(GCS,'on')
    lref=reflength(Nodes);
    set(haxis,'NextPlot','add');
    h=plotgcs(lref,h);
end

% RESET NEXTPLOT STATE
set(haxis,'NextPlot',nextplot);

% RETURN OUTPUT ARGUMENTS ONLY IF REQUESTED
if nargout<1, clear('h'); end

%-------------------------------------------------------------------------------

% CUT PARAMETER FROM LIST
function [value,paramlist]=cutparam(name,default,paramlist);
value=default;
for iarg=length(paramlist)-1:-1:1
    if strcmpi(name,paramlist{iarg})
        value=paramlist{iarg+1};
        paramlist=paramlist([1:iarg-1 iarg+2:end]);
        break
    end
end
