function [pxyz,pind,pvalue]=patch_beam(Nodes,NodeNum,Values)

nElem=size(NodeNum,1);

pxyz = zeros(2*nElem,3);


for iElem=1:nElem
    for ind = 1:2  
    loc=find(Nodes(:,1)==NodeNum(iElem,ind));
    if isempty(loc)
        error('Node %i is not defined.',NodeNum(iElem,1))
    elseif length(loc)>1
        error('Node %i is multiply defined.',NodeNum(iElem,1))
    else
        pxyz(2*(iElem-1)+ind,1)=Nodes(loc,2);
        pxyz(2*(iElem-1)+ind,2)=Nodes(loc,3);
        pxyz(2*(iElem-1)+ind,3)=Nodes(loc,4);
    end
    end
   
end

pind = reshape((1:2*nElem),2,nElem);
pind = pind.';

pvalue = nan(size(pxyz,1),1);