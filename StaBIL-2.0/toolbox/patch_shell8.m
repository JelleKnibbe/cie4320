function [pxyz,pind,pvalue]=patch_shell8(Nodes,NodeNum,Values)

%PATCH_SHELL8  Patch information of the shell8 elements for plotting.
%
%   [pxyz,pind,pvalue] = patch_shell8(Nodes,NodeNum,Values) returns matrices
%   to plot patches of shell8 elements.
%
%   Nodes      Node definitions        [NodID x y z]
%   NodeNum    Node numbers       [NodID1 NodID2 NodID3 NodID4] (nElem * 8)
%   Values     Values assigned to nodes used for coloring    (nElem * 8)
%   pxyz       Coordinates of Nodes                          (8*nElem * 3)
%   pind       indices of Nodes                                (nElem * 8)
%   pvalue     Values arranged per Node                      (8*nElem * 1)
%
%   See also PLOTSTRESSCONTOURF, PLOTSHELLFCONTOURF.

% Miche Jansen
% 2010

nElem=size(NodeNum,1);
nNode = size(NodeNum,2);
nPoints = 5;

Xn=zeros(nNode,nElem);
Yn=zeros(nNode,nElem);
Zn=zeros(nNode,nElem);


N = zeros(4*(nPoints),8);
s = linspace(-1,1,nPoints).';
st = [s -ones(nPoints,1);
      ones(nPoints,1) s;
      -s ones(nPoints,1);
      -ones(nPoints,1) -s];

for ind = 1:4*(nPoints)
    Ni = sh_qs8(st(ind,1),st(ind,2));
    N(ind,:) = Ni; 
end

if size(Values,2) == 4
    Values = [Values(:,1),Values(:,2),Values(:,3),Values(:,4),(Values(:,1)+Values(:,2))/2,...
              (Values(:,2)+Values(:,3))/2,(Values(:,3)+Values(:,4))/2,(Values(:,4)+Values(:,1))/2];
elseif size(Values,2) ~= 8
    error('Size(Values) should be equal to size(NodeNum).');
end

Values=Values.';

for iElem=1:nElem
    for ind =1:8 
    loc=find(Nodes(:,1)==NodeNum(iElem,ind));
    if isempty(loc)
        error('Node %i is not defined.',NodeNum(iElem,1))
    elseif length(loc)>1
        error('Node %i is multiply defined.',NodeNum(iElem,1))
    else
        Xn(ind,iElem)=Nodes(loc,2);
        Yn(ind,iElem)=Nodes(loc,3);
        Zn(ind,iElem)=Nodes(loc,4);
    end
    end
   
end

px=N*Xn;
py=N*Yn;
pz=N*Zn;

pvalue=N*Values;

pind = reshape((1:size(N,1)*nElem),size(N,1),nElem);
pind = pind.';

pxyz = [px(:),py(:),pz(:)];

pvalue = pvalue(:);

end