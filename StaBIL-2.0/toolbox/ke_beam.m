function [Ke,Me] = ke_beam(Node,Section,Material,Options)

%KE_BEAM   Beam element stiffness and mass matrix in global coordinate system.
%
%   [Ke,Me] = ke_beam(Node,Section,Material,Options) returns the element
%   stiffness and mass matrix in the global coordinate system 
%   for a two node beam element (isotropic material)
%
%   Node       Node definitions           [x y z] (3 * 3)
%   Section    Section definition         [A ky kz Ixx Iyy Izz]
%   Material   Material definition        [E nu rho]
%   Options    Element options            {Option1 Option2 ...}
%   Ke         Element stiffness matrix (12 * 12)
%   Me         Element mass matrix (12 * 12)
%
%   See also KELCS_BEAM, TRANS_BEAM, ASMKM, KE_TRUSS.

% David Dooms
% March 2008

% Check nodes
if ~ all(isfinite(Node(1:3,1:3)))
    error('Not all the nodes exist.')
end

% Element length
L=norm(Node(2,:)-Node(1,:)); 

% Material
E=Material(1);
nu=Material(2);

% Section
A=Section(1);
ky=Section(2);
kz=Section(3);
Ixx=Section(4);
Iyy=Section(5);
Izz=Section(6);

% Transformation matrix
t=trans_beam(Node); 
T=blkdiag(t,t,t,t);

if nargout>1            % stiffness and mass
    if nargin<4
    Options={};
    end
    rho=Material(3);
    [KeLCS,MeLCS]=kelcs_beam(L,A,ky,kz,Ixx,Iyy,Izz,E,nu,rho,Options);
    Me=T.'*MeLCS*T;
    Ke=T.'*KeLCS*T;
else                    % only stiffness
    KeLCS=kelcs_beam(L,A,ky,kz,Ixx,Iyy,Izz,E,nu);
    Ke=T.'*KeLCS*T;
end
