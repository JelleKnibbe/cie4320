function F = loads_shell4(DLoad,Node)

%LOADS_SHELL4   Equivalent nodal forces for a shell4 element in the GCS.
%
%   F = loads_shell4(DLoad,Node)
%   computes the equivalent nodal forces of a distributed load 
%   (in the global coordinate system).
%
%   DLoad      Distributed loads      [n1globalX; n1globalY; n1globalZ; ...]
%              in corner Nodes         (12 * 1)
%   Node       Node definitions       [x y z] (4 * 3)
%   F          Load vector  (24 * 1)
%
%   See also LOADSLCS_BEAM, ELEMLOADS, LOADS_TRUSS.

% Miche Jansen
% 2009

[t,Node_lc]=trans_shell4(Node);

T=blkdiag(t,t,t,t);

DLoadLCS=T*DLoad;

FLCS=loadslcs_shell4(DLoadLCS,Node_lc);

T=blkdiag(t,t,t,t,t,t,t,t);

F=T.'*FLCS; 

end