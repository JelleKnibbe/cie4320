function [K,M]=asmkm(Nodes,Elements,Types,Sections,Materials,DOF)

%ASMKM   Assemble stiffness and mass matrix.
%
%   [K,M] = asmkm(Nodes,Elements,Types,Sections,Materials,DOF)
%    K    = asmkm(Nodes,Elements,Types,Sections,Materials,DOF)
%   assembles the stiffness and the mass matrix using the finite element method.
%
%   Nodes      Node definitions          [NodID x y z]
%   Elements   Element definitions       [EltID TypID SecID MatID n1 n2 ...]
%   Types      Element type definitions  {TypID EltName Option1 ... }
%   Sections   Section definitions       [SecID SecProp1 SecProp2 ...]
%   Materials  Material definitions      [MatID MatProp1 MatProp2 ... ]
%   DOF        Degrees of freedom  (nDOF * 1)
%   K          Stiffness matrix (nDOF * nDOF)
%   M          Mass matrix (nDOF * nDOF)
%
%   See also KE_TRUSS, KE_BEAM.


DOF = DOF(:);
nElem=size(Elements,1);
nDOF=length(DOF);
[aDOF,elemInd] = getdof(Elements,Types);

if length(unique(DOF))~=nDOF
    for iDOF = 1:nDOF
        loc = find(abs(DOF(iDOF)-DOF)<1e-4);
        if length(loc)>1
            error('Degree of freedom %.2f is multiply defined.',DOF(iDOF,1))
        end
    end
end
[dum,adofInd] = ismember(aDOF,DOF);

for iElem = 1:nElem
    
    TypID=Elements(iElem,2);
    loc=find(cell2mat(Types(:,1))==TypID);
    if isempty(loc)
        error('Element type %i is not defined.',TypID)
    elseif length(loc)>1
        error('Element type %i is multiply defined.',TypID)
    end
    
    Type=Types{loc,2};
    
    if size(Types,2)<3
        Options={};
    else
        Options=Types{loc,3};
    end
    
    SecID=Elements(iElem,3);
    loc=find(Sections(:,1)==SecID);
    if isempty(loc)
        error('Section %i is not defined.',SecID)
    elseif length(loc)>1
        error('Section %i is multiply defined.',SecID)
    end
    Section=Sections(loc,2:end);
    
    MatID=Elements(iElem,4);
    loc=find(Materials(:,1)==MatID);
    if isempty(loc)
        error('Material %i is not defined.',MatID)
    elseif length(loc)>1
        error('Material %i is multiply defined.',MatID)
    end
    Material=Materials(loc,2:end);
     
    NodeNum=Elements(iElem,5:end);
    
    Node=zeros(length(NodeNum),3);
    for iNode=1:length(NodeNum)
        loc=find(Nodes(:,1)==NodeNum(1,iNode));
        if isempty(loc)
            Node(iNode,:)=[NaN NaN NaN];
        elseif length(loc)>1
            error('Node %i is multiply defined.',NodeNum(1,iNode))
        else
            Node(iNode,:)=Nodes(loc,2:end);
        end
    end
    
    inddofelem = elemInd{iElem}.';
    
    utilind=find(inddofelem);
    nutilind=length(utilind);
    indi{iElem}=reshape(repmat(inddofelem(utilind),1,nutilind),1,nutilind^2);
    indj{iElem}=reshape(repmat(inddofelem(utilind),nutilind,1),1,nutilind^2);   
    
    try
    if nargout>1            % stiffness and mass
        [Ke,Me]=eval(['ke_' Type '(Node,Section,Material,Options)']);
        sM{iElem}=reshape(Me(utilind,utilind),1,nutilind^2);
        sK{iElem}=reshape(Ke(utilind,utilind),1,nutilind^2);
    else                    % only stiffness
        Ke=eval(['ke_' Type '(Node,Section,Material,Options)']);
        sK{iElem}=reshape(Ke(utilind,utilind),1,nutilind^2);
    end
    catch lerr
        error('Element on line %i: %s',iElem,lerr.message)
    end
    
    
end
indi=adofInd(cell2mat(indi));
indj=adofInd(cell2mat(indj));
prdof = and(logical(indj),logical(indi));

sK=cell2mat(sK);

K=sparse(indi(prdof),indj(prdof),sK(prdof),nDOF,nDOF);
K=(K+K.')/2;            % make the matrix exactly symmetric

if nargout>1  % stiffness and mass
    sM=cell2mat(sM);
    M=sparse(indi(prdof),indj(prdof),sM(prdof),nDOF,nDOF);
    M=(M+M.')/2;        % make the matrix exactly symmetric
end

end