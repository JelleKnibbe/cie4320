function [Ax,Ay,Az,B,Cx,Cy,Cz]=disp_shell8(Nodes,Elements,DOF,EltIDDLoad,Sections,Materials,nPoints)

%DISP_SHELL8   Matrices to compute the displacements of the deformed shell.
%
%   [Ax,Ay,Az,B] = disp_shell8(Nodes,Elements,DOF,U)
%   returns the matrices to compute the displacements of the deformed shell.
%   The coordinates of the nodes of the shell8 element are 
%   computed using X=Ax*U+B(:,1); Y=Ay*U+B(:,2) and 
%   Z=Az*U+B(:,3). 
%
%   Nodes      Node definitions        [NodID x y z]
%   Elements   Element definitions     [EltID TypID SecID MatID n1 n2 ...]
%   DOF        Degrees of freedom  (nDOF * 1)
%   Ax         Matrix to compute the x-coordinates of the deformations 
%   Ay         Matrix to compute the y-coordinates of the deformations 
%   Az         Matrix to compute the z-coordinates of the deformations 
%   B          Matrix which contains the x-, y- and z-coordinates of the
%              undeformed structure
%
%   See also DISP_TRUSS, PLOTDISP, DISP_SHELL4.

% Miche Jansen
% 2009

if nargin<7
    nPoints=3;
    N = zeros(4*(nPoints),8);
    s = linspace(-1,1,nPoints).';
    st = [s -ones(nPoints,1);
          ones(nPoints,1) s;
          -s ones(nPoints,1);
          -ones(nPoints,1) -s]; %nPoints minstens 2 !!!
else
     N = zeros(4*(nPoints),8);
     s = linspace(-1,1,nPoints).';
     st = [s -ones(nPoints,1);
           ones(nPoints,1) s;
           -s ones(nPoints,1);
           -ones(nPoints,1) -s];
end

nElem=size(Elements,1);
nDOF=size(DOF,1);


for i = 1:4*(nPoints)
    Ni = sh_qs8(st(i,1),st(i,2));
    N(i,:) = Ni; 
end

%Ax=sparse([],[],[],nElem*(4*nPoints+1),nDOF);
%Ay=sparse([],[],[],nElem*(4*nPoints+1),nDOF);
%Az=sparse([],[],[],nElem*(4*nPoints+1),nDOF);
B=zeros(nElem*(4*nPoints+1),3);

Ax=sparse([],[],[],nElem*(4*nPoints+1),24*nElem);
Ay=sparse([],[],[],nElem*(4*nPoints+1),24*nElem);
Az=sparse([],[],[],nElem*(4*nPoints+1),24*nElem);

Nx = zeros(4*nPoints,24);
Ny = zeros(4*nPoints,24);
Nz = zeros(4*nPoints,24);

% select translation-dofs
indtr = [1,2,3,7,8,9,13,14,15,19,20,21,25,26,27,31,32,33,37,38,39,43,44,45];

Nx(:,1:3:22) = N;
Ny(:,2:3:23) = N;
Nz(:,3:3:24) = N;

Ca = cell(nElem,1);

for iElem = 1:nElem
    
    NodeNum=Elements(iElem,5:end);
    
    Node=zeros(length(NodeNum),3);
    for iNode=1:length(NodeNum)
        loc=find(Nodes(:,1)==NodeNum(1,iNode));
        if isempty(loc)
            Node(iNode,:)=[NaN NaN NaN];
        elseif length(loc)>1
            error('Node %i is multiply defined.',NodeNum(1,iNode))
        else
            Node(iNode,:)=Nodes(loc,2:end);
        end
    end
    
    dofelem=dof_shell8(NodeNum);
    dofelem = dofelem(indtr);
    
    C=selectdof(DOF,dofelem);
    
    B((1:4*nPoints)+(iElem-1)*(4*nPoints+1),1) = N*Node(:,1);
    B((1:4*nPoints)+(iElem-1)*(4*nPoints+1),2) = N*Node(:,2);
    B((1:4*nPoints)+(iElem-1)*(4*nPoints+1),3) = N*Node(:,3);

    B(iElem*(4*nPoints+1),:) = NaN;
    

    %Ax((1:4*nPoints)+(iElem-1)*(4*nPoints+1),1:nDOF) = Nx*C;
    %Ay((1:4*nPoints)+(iElem-1)*(4*nPoints+1),1:nDOF) = Ny*C;
    %Az((1:4*nPoints)+(iElem-1)*(4*nPoints+1),1:nDOF) = Nz*C;
    
    Ca{iElem,1} = C; % met cell-array werken voor snelheid
    Ax((1:4*nPoints)+(iElem-1)*(4*nPoints+1),24*(iElem-1)+(1:24)) = Nx;
    Ay((1:4*nPoints)+(iElem-1)*(4*nPoints+1),24*(iElem-1)+(1:24)) = Ny;    
    Az((1:4*nPoints)+(iElem-1)*(4*nPoints+1),24*(iElem-1)+(1:24)) = Nz;
    
    
end

Ca = cell2mat(Ca);
Ax = Ax*Ca;
Ay = Ay*Ca;
Az = Az*Ca;

if nargout > 4
    Cx = 0;
    Cy = 0; % er wordt geen rekening gehouden met de vervormingen door de verdeelde belastingen
    Cz = 0;
end
end
