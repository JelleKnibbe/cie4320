function t = trans_truss(Node)

%TRANS_TRUSS   Transform coordinate system for a truss element.
%
%   t = trans_truss(Node)
%   computes the transformation matrix between the local and the global
%   coordinate system for the truss element.
%
%   Node       Node definitions           [x y z] (2 * 3)
%   t          Transformation matrix  (3 * 3)
%
%   See also KE_TRUSS, TRANS_BEAM.

% David Dooms
% March 2008


Nx=Node(2,:)-Node(1,:);        % vector along the local x-axis
Nx=Nx/norm(Nx);                % normalized vector along the local x-axis

[delta,index]=min(abs(fliplr(Nx)));
V13=zeros(1,3);
V13(1,(4-index))=-1;                % vector between nodes 1 and 3

Ny=cross(Nx,V13);              % vector along the local z-axis
Ny=Ny/norm(Ny);                % normalized vector along the local z-axis
Nz=cross(Nx,Ny);               % normalized vector along the local y-axis

t=[Nx; Ny; Nz];
