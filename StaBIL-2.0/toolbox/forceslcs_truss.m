function Forces=forceslcs_truss(KeLCS,UeLCS)

%FORCESLCS_TRUSS   Compute the element forces for a truss element in the LCS.
%
%   Forces=forceslcs_truss(KeLCS,UeLCS)
%   computes the element forces for the truss element in the local coordinate 
%   system (algebraic convention).
%
%   KeLCS      Element stiffness matrix (6 * 6)
%   UeLCS      Displacements (6 * 1)
%   Forces     Element forces          [N; 0; 0](6 * 1)
%
%   See also FORCES_TRUSS, FORCESLCS_BEAM

% David Dooms
% October 2008

% compute element forces
Forces=KeLCS*UeLCS;
