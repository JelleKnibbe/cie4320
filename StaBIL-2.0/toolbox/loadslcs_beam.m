function FLCS = loadslcs_beam(DLoadLCS,L)

%LOADSLCS_BEAM   Equivalent nodal forces for a beam element in the LCS.
%
%   FLCS = loadslcs_beam(DLoadLCS,L)
%   computes the equivalent nodal forces of a distributed load 
%   (in the local coordinate system).
%
%   DLoadLCS   Distributed loads        [n1localX; n1localY; n1localZ; ...]
%                                                                   (6 * 1)
%   L          Beam length 
%   FLCS       Load vector  (12 * 1)
%
%   See also LOADS_BEAM.

% David Dooms
% March 2008

FLCS=zeros(12,size(DLoadLCS,2));

% loads in the local x-direction
FLCS(1,:)=L/6*(2*DLoadLCS(1,:)+DLoadLCS(4,:));
FLCS(7,:)=L/6*(DLoadLCS(1,:)+2*DLoadLCS(4,:));

% fout zoals in calm
% FLCS(1,:)=L/20*(7*DLoadLCS(1,:)+3*DLoadLCS(4,:));
% FLCS(7,:)=L/20*(3*DLoadLCS(1,:)+7*DLoadLCS(4,:));

% loads in the local y-direction
FLCS(2,:)=L/20*(7*DLoadLCS(2,:)+3*DLoadLCS(5,:));
FLCS(8,:)=L/20*(3*DLoadLCS(2,:)+7*DLoadLCS(5,:));
FLCS(6,:)=L^2/60*(3*DLoadLCS(2,:)+2*DLoadLCS(5,:));
FLCS(12,:)=-L^2/60*(2*DLoadLCS(2,:)+3*DLoadLCS(5,:));

% loads in the local z-direction
FLCS(3,:)=L/20*(7*DLoadLCS(3,:)+3*DLoadLCS(6,:));
FLCS(9,:)=L/20*(3*DLoadLCS(3,:)+7*DLoadLCS(6,:));
FLCS(5,:)=-L^2/60*(3*DLoadLCS(3,:)+2*DLoadLCS(6,:));
FLCS(11,:)=L^2/60*(2*DLoadLCS(3,:)+3*DLoadLCS(6,:));
