function [Nodes,Elements,Edge1,Edge2,Edge3,Edge4] = makemesh(Line1,Line2,Line3,Line4,m,n,Type,Section,Material,varargin)

%MAKEMESH   Creates a mesh of quadrilateral elements for a surface defined by 4 lines
%
%   [Nodes,Elements,Edge1,Edge2,Edge3,Edge4] =
%   makemesh(Line1,Line2,Line3,Line4,m,n,Type,Section,Material)
%   
%   Line1,2,3,4 Lines that define the surface. (n1 * 3)
%               These lines:
%                   -should be defined in a clockwise or counter-clockwise order
%                    (this means: first point of Line2 is the last point of Line1
%                    in the case of a counter-clockwise direction)
%                   -can be defined by any number of points
%                   
%   m           Number of elements that divide Line2 and Line4
%   n           Number of elements that divide Line1 and Line3
%   Type        Cell containing Type number and name        {TypeID EltName}
%   Section     Section ID
%   Material    Material ID
%
%   Nodes       Nodes definitions       [NodID x y z]
%   Elements    Element definitions     [EltID TypID SecID MatID n1 n2 ...]
%   Edge1,2,3,4 vector containing Nodes on Line1,2,..  ((m+1) * 1) or ((n+1) * 1)
%   
%   makemesh(...,ParamName,ParamValue)sets the value of the specified
%   parameters.  The following parameters can be specified:
%   'L1method'   uses the specified interpolating method for Line1.
%                default: 'spline'. This method is used in the MATLAB
%                INTERP1 function. See DOC INTERP1 for all
%                possibilities.
%   'L2method' 
%   'L3method'   
%   'L4method'   
%
%   See also GRID_SHELL8, GRID_SHELL4.

% Miche Jansen
% 2009

% PARAMETERS

if nargin > 9
paramlist=varargin;
else paramlist = [];
end

[L1interpMode,paramlist]=cutparam('L1method','spline',paramlist);
[L2interpMode,paramlist]=cutparam('L2method','spline',paramlist);
[L3interpMode,paramlist]=cutparam('L3method','spline',paramlist);
[L4interpMode,paramlist]=cutparam('L4method','spline',paramlist);

TypeName = Type{1,2};

[s,t,NodeNum,Elements] = eval(['grid_' TypeName '(m,n,Type,Section,Material)']);
    

s1 = linspace(-1,1,size(Line1,1));
L1 = interp1(s1,Line1,s,L1interpMode);


s2 = linspace(-1,1,size(Line2,1));
L2 = interp1(s2,Line2,t,L2interpMode);

s3 = linspace(-1,1,size(Line3,1));
L3 = interp1(s3,Line3,-s,L3interpMode);

s4 = linspace(-1,1,size(Line4,1));
L4 = interp1(s4,Line4,-t,L4interpMode);

L1 = L1.';
L2 = L2.';
L3 = L3.';
L4 = L4.';

N1_s = (1-s)/2;
N1_t = (1-t)/2;
N2_s = (1+s)/2;
N2_t = (1+t)/2;

P_tx = N1_t.'*L1(1,:)+N2_t.'*L3(1,:);
P_ty = N1_t.'*L1(2,:)+N2_t.'*L3(2,:);
P_tz = N1_t.'*L1(3,:)+N2_t.'*L3(3,:);

P_sx = L4(1,:).'*N1_s+L2(1,:).'*N2_s;
P_sy = L4(2,:).'*N1_s+L2(2,:).'*N2_s;
P_sz = L4(3,:).'*N1_s+L2(3,:).'*N2_s;

% de volgende formules gaan ervan uit dat de beginpunten van lijn 1 en 3
% gelijk zijn aan de eindpunten van lijn 2 en 4 en omgekeerd
P_stx = N1_t.'*N1_s*Line1(1,1)+N2_t.'*N1_s*Line3(end,1)+ ...
        N1_t.'*N2_s*Line1(end,1)+N2_t.'*N2_s*Line3(1,1);
P_sty = N1_t.'*N1_s*Line1(1,2)+N2_t.'*N1_s*Line3(end,2)+ ...
        N1_t.'*N2_s*Line1(end,2)+N2_t.'*N2_s*Line3(1,2);    
P_stz = N1_t.'*N1_s*Line1(1,3)+N2_t.'*N1_s*Line3(end,3)+ ...
        N1_t.'*N2_s*Line1(end,3)+N2_t.'*N2_s*Line3(1,3);    

phi_x = P_tx+P_sx-P_stx;
phi_y = P_ty+P_sy-P_sty;
phi_z = P_tz+P_sz-P_stz;

Nodes = [];
for j=1:size(phi_x,2)
    for i=1:size(phi_x,1)
        if ~isnan(NodeNum(i,j))
        Nodes = [Nodes; NodeNum(i,j) phi_x(i,j) phi_y(i,j) phi_z(i,j)];
        end
    end
end

if nargout > 2
    Edge4 = NodeNum(:,1);
    Edge1 = NodeNum(end,:).';
    Edge2 = NodeNum(:,end);
    Edge3 = NodeNum(1,:).';
end



%-------------------------------------------------------------------------------

% CUT PARAMETER FROM LIST
function [value,paramlist]=cutparam(name,default,paramlist);
value=default;
for iarg=length(paramlist)-1:-1:1
    if strcmpi(name,paramlist{iarg})
        value=paramlist{iarg+1};
        paramlist=paramlist([1:iarg-1 iarg+2:end]);
        break
    end
end




