function t = trans_beam(Node)

%TRANS_BEAM   Transform coordinate system for a beam element.
%
%   t = trans_beam(Node)
%   computes the transformation matrix between the local and the global
%   coordinate system for the beam element.
%
%   Node       Node definitions           [x y z] (3 * 3)
%   t          Transformation matrix  (3 * 3)
%
%   See also KE_BEAM, TRANS_TRUSS.

% David Dooms
% March 2008

Nx=Node(2,:)-Node(1,:);        % vector along the local x-axis
Nx=Nx/norm(Nx);                % normalized vector along the local x-axis

V13=Node(3,:)-Node(1,:);       % vector between nodes 1 and 3

Nz=cross(Nx,V13);              % vector along the local z-axis
if Nz==0
    error('The three nodes of the beam element are collinear')
end
Nz=Nz/norm(Nz);                % normalized vector along the local z-axis
Ny=cross(Nz,Nx);               % normalized vector along the local y-axis

t=[Nx; Ny; Nz];
