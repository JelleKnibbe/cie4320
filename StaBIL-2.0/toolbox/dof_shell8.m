function dof = dof_shell8(NodeNum)

%DOF_SHELL8   Element degrees of freedom for a shell8 element.
%
%   dof = dof_shell8(NodeNum) builds the vector with the 
%   labels of the degrees of freedom for which stiffness is present in the 
%   shell8 element.
%
%   NodeNum Node definitions           [NodID1 NodID2 ... NodIDn]   (1 * 8)
%   dof     Degrees of freedom                                     (48 * 1)       
%
%   See also GETDOF.



dof=zeros(6*length(NodeNum),1);
for ind = 1:length(NodeNum)
    dof((1:6)+6*(ind-1))=NodeNum(ind)+[0.01; 0.02; 0.03; 0.04; 0.05; 0.06];
end

end



