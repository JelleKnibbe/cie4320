function [KeLCS,MeLCS] = kelcs_truss(L,A,E,rho,Options)

%KELCS_TRUSS   Truss element stiffness and mass matrix in local coordinate system.
%
%   [KeLCS,MeLCS] = kelcs_truss(L,A,E,rho,Options) 
%    KeLCS        = kelcs_truss(L,A,E)
%   returns the element stiffness and mass matrix in the local coordinate system  
%   for a two node truss element (isotropic material)
%
%   L          Truss length 
%   A          Truss cross section
%   E          Young's modulus
%   rho        Mass density
%   Options    Options for the mass matrix: {'lumped'}
%   KeLCS      Element stiffness matrix (6 * 6)
%   MeLCS      Element mass matrix (6 * 6)
%
%   See also KE_TRUSS, KELCS_BEAM.

% David Dooms
% March 2008

%% STIFFNESS MATRIX %%

KeLCS=sparse([],[],[],6,6,4);

KeLCS(1,1)=E*A/L;
KeLCS(4,4)=KeLCS(1,1);
KeLCS(4,1)=-KeLCS(1,1);

% symmetry
KeLCS(1,4)=KeLCS(4,1);

%% MASS MATRIX %%

if nargout>1 
    
    if nargin<5
        Options={};
    end

    if ~ any(strcmpi(Options,'lumped'))     % consistent mass matrix

        MeLCS=sparse([],[],[],6,6,12);

        MeLCS(1,1)=rho*A*L/3;
        MeLCS(2,2)=MeLCS(1,1);
        MeLCS(3,3)=MeLCS(1,1);
        MeLCS(4,4)=MeLCS(1,1);
        MeLCS(5,5)=MeLCS(1,1);
        MeLCS(6,6)=MeLCS(1,1);

        MeLCS(4,1)=rho*A*L/6;
        MeLCS(5,2)=MeLCS(4,1);
        MeLCS(6,3)=MeLCS(4,1);

        % symmetry
        MeLCS(1,4)=MeLCS(4,1);
        MeLCS(2,5)=MeLCS(5,2);
        MeLCS(3,6)=MeLCS(6,3);

    else                                    % lumped mass matrix
        
        MeLCS=sparse([],[],[],6,6,6);

        MeLCS(1,1)=rho*A*L/2;
        MeLCS(2,2)=MeLCS(1,1);
        MeLCS(3,3)=MeLCS(1,1);
        MeLCS(4,4)=MeLCS(1,1);
        MeLCS(5,5)=MeLCS(1,1);
        MeLCS(6,6)=MeLCS(1,1);
        
    end

end
