function [Ax,Ay,Az,B,Cx,Cy,Cz]=disp_truss(Nodes,Elements,DOF,EltIDDLoad,Sections,Materials,Points)

%DISP_TRUSS  Return matrices to compute the displacements of the deformed trusses.
%
%   [Ax,Ay,Az,B]=disp_truss(Nodes,Elements,DOF,[],[],[],Points)
%   [Ax,Ay,Az,B]=disp_truss(Nodes,Elements,DOF)
%   returns the matrices to compute the displacements of the deformed trusses.
%   The coordinates of the specified points along the deformed beams element are 
%   computed using X=Ax*U+B(:,1); Y=Ay*U+B(:,2) and Z=Az*U+B(:,3).
%
%   Nodes      Node definitions        [NodID x y z]
%   Elements   Element definitions     [EltID TypID SecID MatID n1 n2 ...]
%   DOF        Degrees of freedom  (nDOF * 1)
%   Points     Points in the local coordinate system (1 * nPoints)
%   Ax         Matrix to compute the x-coordinates of the deformations 
%   Ay         Matrix to compute the y-coordinates of the deformations 
%   Az         Matrix to compute the z-coordinates of the deformations 
%   B          Matrix which contains the x-, y- and z-coordinates of the 
%              undeformed structure
%
%   See also DISP_BEAM, PLOTDISP.

% David Dooms
% March 2008

% PREPROCESSING
DOF=DOF(:);

if nargin<7
    nPoints=2;
    Points=linspace(0,1,nPoints);
else
    Points=Points(:).';
    nPoints=size(Points,2);
end

A=[-1  1;
   -1  1;
   -1  1;
    1  0;
    1  0;
    1  0];

NeLCS=zeros(size(Points,2),2);

for k=1:6
    NeLCS(:,k)=polyval(A(k,:),Points);
end

nElem=size(Elements,1);
nDOF=size(DOF,1);

Ax=sparse([],[],[],nElem*(nPoints+1),nDOF,nElem*nPoints*12+nElem);
Ay=sparse([],[],[],nElem*(nPoints+1),nDOF,nElem*nPoints*12+nElem);
Az=sparse([],[],[],nElem*(nPoints+1),nDOF,nElem*nPoints*12+nElem);

B=zeros(nElem*(nPoints+1),3);

for iElem=1:nElem
    
    NodeNum=Elements(iElem,5:end);
    
    Node=zeros(length(NodeNum),3);
    for iNode=1:length(NodeNum)
        loc=find(Nodes(:,1)==NodeNum(1,iNode));
        if isempty(loc)
            Node(iNode,:)=[NaN NaN NaN];
        elseif length(loc)>1
            error('Node %i is multiply defined.',NodeNum(1,iNode))
        else
            Node(iNode,:)=Nodes(loc,2:end);
        end
    end
    
    dofelem=dof_truss(NodeNum);
    
    C=selectdof(DOF,dofelem);

    Ax(((nPoints+1)*(iElem-1)+1):((nPoints+1)*iElem-1),1:nDOF)=NeLCS*diag([1 0 0 1 0 0])*C;
    Ay(((nPoints+1)*(iElem-1)+1):((nPoints+1)*iElem-1),1:nDOF)=NeLCS*diag([0 1 0 0 1 0])*C;
    Az(((nPoints+1)*(iElem-1)+1):((nPoints+1)*iElem-1),1:nDOF)=NeLCS*diag([0 0 1 0 0 1])*C;
    
    B(((nPoints+1)*(iElem-1)+1):((nPoints+1)*iElem-1),1:3)=[NeLCS(:,1), NeLCS(:,4)]*Node(1:2,:);
    
    B((nPoints+1)*iElem,1)=NaN;

end

if nargout>4
    nDLoad=size(EltIDDLoad,1);
    Cx=sparse([],[],[],nElem*(nPoints+1),nDLoad*6,0);
    Cy=sparse([],[],[],nElem*(nPoints+1),nDLoad*6,0);
    Cz=sparse([],[],[],nElem*(nPoints+1),nDLoad*6,0);
end
