function [l,d]=ldl(k);

%LDL Gauss factorization of symmetric matrices.
%
%   [L,D] = LDL(A) returns the triangle matrix L and the vector D so that
%   A = L.' * diag(D) * L.  You should consider renumbering A with SYMRCM before
%   factoring the matrix.  See details in K.J. Bathe, 'Finite Element Procedures
%   in Engineering Analysis', Prentice-Hall, 1982, p. 442

% Etienne Balmes  05/05/97
% Copyright (c) 1990-1997 by Etienne Balmes
% All Rights Reserved.

i3=zeros(1,size(k,1));
for j1=1:size(k,1); i3(j1) = min(find(k(:,j1))); end

l=spalloc(size(k,1),size(k,2),sum([1:size(k,1)]-i3));
d=zeros(size(k,1),1);

d(1,1)=k(1,1);l(1,1)=1;g=zeros(1,size(k,1));
time = cputime;

for j1=2:size(k,1)
  g(:)=0; g(i3(j1))=k(i3(j1),j1);
  for j2=i3(j1):j1-1
    in2 = max(i3(j1),i3(j2)):j2-1;
    if ~isempty(in2)  g(j2) = k(j2,j1)-g(in2)*l(in2,j2);
    else g(j2) = k(j2,j1); end
  end
  in1 = i3(j1):j1-1;
  if ~isempty(in1) l(in1,j1)=(g(in1).'./d(in1));
                   d(j1) = k(j1,j1)-g(in1)*l(in1,j1);
  else d(j1)=k(j1,j1);end
  l(j1,j1)=1;
  if cputime-time>300 fprintf(1,' %i',j1);time=cputime;end
end
