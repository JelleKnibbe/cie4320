function [Ke,Me] = ke_truss(Node,Section,Material,Options)

%KE_TRUSS   Truss element stiffness and mass matrix in global coordinate system.
%
%   [Ke,Me] = ke_truss(Node,Section,Material,Options) returns the element
%   stiffness and mass matrix in the global coordinate system 
%   for a two node truss element (isotropic material)
%
%   Node       Node definitions           [x1 y1 z1; x2 y2 z2] (2 * 3)
%   Section    Section definition         [A]
%   Material   Material definition        [E nu rho]
%   Options    Element options            {Option1 Option2 ...}
%   Ke         Element stiffness matrix   (6 * 6)
%   Me         Element mass matrix        (6 * 6)
%
%   See also KELCS_TRUSS, TRANS_TRUSS, ASMKM, KE_BEAM.

% David Dooms
% March 2008

% Check nodes
if ~ all(isfinite(Node(1:2,1:3)))
    error('Not all the nodes exist.')
end

% Element length
L=norm(Node(2,:)-Node(1,:)); 

% Material
E=Material(1);

% Section
A=Section(1);

% Transformation matrix 
t=trans_truss(Node);
T=blkdiag(t,t);

if nargout>1            % stiffness and mass
    if nargin<4
        Options={};
    end
    rho=Material(3);
    [KeLCS,MeLCS]=kelcs_truss(L,A,E,rho,Options);
    Me=full(MeLCS);
    Ke=T.'*KeLCS*T;
else                    % only stiffness
    KeLCS=kelcs_truss(L,A,E);
    Ke=T.'*KeLCS*T;
end
