function h = plotlcs(Nodes,Elements,Types,vLCS,varargin)

%PLOTLCS   Plot the local element coordinate system for shell elements.
%
%   plotlcs(Nodes,Elements,Types,vLCS,varargin)
%
%   Nodes      Node definitions        [NodID x y z]
%   Elements   Element definitions     [EltID TypID SecID MatID n1 n2 ...]
%   Types      Element type definitions  {TypID EltName Option1 ... }
%   vLCS       Element coordinate systems (nElem * 9)
%   plotlcs(...,ParamName,ParamValue) sets the value of the specified
%   parameters.  The following parameters can be specified:
%   'GCS'          Plot the GCS. Default: 'on'.
%   'Undeformed'   Plots the undeformed mesh.  Default: 'k-'.
%   'Handle'       Plots in the axis with this handle.  Default: current
%   axis.
%
%   See also ELEMSTRESS

% PARAMETERS
if nargin<5                               
    paramlist={};
elseif nargin>5 && ischar(varargin{1})
    paramlist=varargin;
end


[GCS,paramlist]=cutparam('GCS','on',paramlist);
[Undeformed,paramlist]=cutparam('Undeformed','k:',paramlist);
[haxis,paramlist]=cutparam('Handle',gca,paramlist);


nElem = size(Elements,1);
x = zeros(1,nElem);
y = zeros(1,nElem);
z = zeros(1,nElem);


for iElem=1:nElem
    
    
    TypID=Elements(iElem,2);
    loc=find(cell2mat(Types(:,1))==TypID);
    if isempty(loc)
        error('Element type %i is not defined.',TypID)
    elseif length(loc)>1
        error('Element type %i is multiply defined.',TypID)
    end
    
    Type=Types{loc,2};
    
    NodeNum=Elements(iElem,5:end);
    
    Node=zeros(length(NodeNum),3);
    for iNode=1:length(NodeNum)
        loc=find(Nodes(:,1)==NodeNum(1,iNode));
        if isempty(loc)
            Node(iNode,:)=[NaN NaN NaN];
        elseif length(loc)>1
            error('Node %i is multiply defined.',NodeNum(1,iNode))
        else
            Node(iNode,:)=Nodes(loc,2:end);
        end
    end
    

% elementen moeten vier knopen hebben voor deze functie !!
x(iElem) = mean(Node(:,1));
y(iElem) = mean(Node(:,2));
z(iElem) = mean(Node(:,3));


end

% PREPARE FIGURE
nextplot=get(haxis,'NextPlot');
set(haxis,'NextPlot','add');
handleLCS(1) = quiver3(haxis,x,y,z,vLCS(:,1)',vLCS(:,2)',vLCS(:,3)',0.25,'r');
handleLCS(2) = quiver3(haxis,x,y,z,vLCS(:,4)',vLCS(:,5)',vLCS(:,6)',0.25,'g');
handleLCS(3) = quiver3(haxis,x,y,z,vLCS(:,7)',vLCS(:,8)',vLCS(:,9)',0.25,'b');

% PLOT UNDEFORMED MESH
if ~ strcmpi(Undeformed,'off')
    h=plotelem(Nodes,Elements,Types,Undeformed,'Numbering','off','Handle',haxis,'GCS','off');
end

h.lcs = handleLCS;

% VIEWPOINT AND AXES FIGURE
if strcmpi(nextplot,'replace')
    if all(Nodes(:,4)==0)
        view(0,90);
    elseif all(Nodes(:,3)==0)
        view(0,0);
    elseif all(Nodes(:,2)==0)
        view(90,0);
    else
        view(37.5,30);
    end
    defaultposition=[0.13 0.11 0.775 0.815];
    if all(get(haxis,'Position')==defaultposition)
        set(haxis,'Position',[0.05 0.05 0.9 0.87])
    end
    axis equal
    set(gcf,'resizefcn',@figResize)
    axis fill
    axis off
    set(gcf,'PaperPositionMode','auto')
end

% PLOT GLOBAL COORDINATE SYSTEM
if strcmpi(GCS,'on')
    lref=reflength(Nodes);
    set(haxis,'NextPlot','add');
    h=plotgcs(lref,h);
end

% RESET NEXTPLOT STATE
set(haxis,'NextPlot',nextplot);

% RETURN OUTPUT ARGUMENTS ONLY IF REQUESTED
if nargout<1, clear('h'); end

%-------------------------------------------------------------------------------

% CUT PARAMETER FROM LIST
function [value,paramlist]=cutparam(name,default,paramlist);
value=default;
for iarg=length(paramlist)-1:-1:1
    if strcmpi(name,paramlist{iarg})
        value=paramlist{iarg+1};
        paramlist=paramlist([1:iarg-1 iarg+2:end]);
        break
    end
end

