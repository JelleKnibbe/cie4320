function DLoads=multdloads(varargin)

%MULTDLOADS   Combine distributed loads.
%
%   DLoads=multdloads(DLoads_1,DLoads_2,...,DLoads_k)
%   combines the distributed loads of multiple load cases into one 3D array.
%   Each plane corresponds to one load case.
%
%   DLoads_k   Distributed loads       [EltID n1globalX n1globalY n1globalZ ...]
%                                                                  (nElem_k * 7)
%   DLoads     Distributed loads       [EltID n1globalX n1globalY n1globalZ ...]
%                                                             (maxnElem * 7 * k)
%
%   See also ELEMLOADS.

% David Dooms
% March 2009

elemnumbers=[];

for n=1:nargin
    DLoadn=varargin{n};
    elemnumbers=[elemnumbers; DLoadn(:,1)];
end

elemnumbers=unique(elemnumbers);

DLoads=zeros(length(elemnumbers),7,nargin);
DLoads(:,1,1)=elemnumbers;
DLoads(:,1,2:nargin)=NaN(length(elemnumbers),1,(nargin-1));

for n=1:nargin
    DLoadn=varargin{n};
    for k=1:size(DLoadn,1)
        DLoads(find(elemnumbers==DLoadn(k,1)),2:7,n)=DLoadn(k,2:7);
    end
end
