function F = loads_truss(DLoad,Node)

%LOADS_TRUSS   Equivalent nodal forces for a truss element in the GCS.
%
%   F = loads_truss(DLoad,Node)
%   computes the equivalent nodal forces of a distributed load 
%   (in the global coordinate system).
%
%   DLoad      Distributed loads          [n1globalX n1globalY n1globalZ ...]
%                                                                     (6 * 1)
%   Node       Node definitions           [x y z] (2 * 3)
%   F          Load vector  (6 * 1)
%
%   See also ELEMLOADS, LOADS_BEAM.

% David Dooms
% October 2008

L=norm(Node(2,:)-Node(1,:));

F=zeros(6,size(DLoad,2));
F(1:3,:)=L/6*(2*DLoad(1:3,:)+DLoad(4:6,:));
F(4:6,:)=L/6*(DLoad(1:3,:)+2*DLoad(4:6,:));
