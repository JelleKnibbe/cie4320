function F = loads_beam(DLoad,Node)

%LOADS_BEAM   Equivalent nodal forces for a beam element in the GCS.
%
%   F = loads_beam(DLoad,Node)
%   computes the equivalent nodal forces of a distributed load 
%   (in the global coordinate system).
%
%   DLoad      Distributed loads     [n1globalX; n1globalY; n1globalZ; ...]
%                                                                   (6 * 1)
%   Node       Node definitions           [x y z] (3 * 3)
%   F          Load vector  (12 * 1)
%
%   See also LOADSLCS_BEAM, ELEMLOADS, LOADS_TRUSS.

% David Dooms
% March 2008

L=norm(Node(2,:)-Node(1,:));

t=trans_beam(Node);

T=blkdiag(t,t);

DLoadLCS=T*DLoad(1:6,:);

FLCS=loadslcs_beam(DLoadLCS,L);

T=blkdiag(t,t,t,t);

F=T.'*FLCS; 
