function [FdiagrLCS,loc,Extrema] = fdiagrlcs_beam(ftype,Forces,DLoadLCS,L,Points)

%FDIAGRLCS_BEAM   Force diagram for a beam element in LCS.
%
%   [FdiagrLCS,loc,Extrema] = fdiagrlcs_beam(ftype,Forces,DLoadLCS,L,Points) 
%   computes the elements forces at the specified points. The extreme values are
%   analytically determined.
%
%   ftype      'norm'       Normal force (in the local x-direction)
%              'sheary'     Shear force in the local y-direction
%              'shearz'     Shear force in the local z-direction
%              'momx'       Torsional moment (around the local x-direction)
%              'momy'       Bending moment around the local y-direction
%              'momz'       Bending moment around the local z-direction
%   Forces     Element forces in LCS (beam convention) [N; Vy; Vz; T; My; Mz](12 * 1)
%   DLoadLCS   Distributed loads in LCS [n1localX; n1localY; n1localZ; ...](6 * 1)
%   Points     Points in the local coordinate system (1 * nPoints)
%   FdiagrLCS  Element forces at the points (1 * nPoints)
%   loc        Locations of the extreme values (nValues * 1)
%   Extrema    Extreme values (nValues * 1)
%
%   See also FDIAGRGCS_BEAM.

% David Dooms
% October 2008

% PREPROCESSING
Points=Points(:).';

switch lower(ftype)
   case 'norm'
      A=[(DLoadLCS(1)-DLoadLCS(4))*L/2;  -DLoadLCS(1)*L;   Forces(1)];
   case 'sheary'
      A=[(DLoadLCS(2)-DLoadLCS(5))*L/2;  -DLoadLCS(2)*L;   Forces(2)];
   case 'shearz'
      A=[(DLoadLCS(3)-DLoadLCS(6))*L/2;  -DLoadLCS(3)*L;   Forces(3)];
   case 'momx'
      A=[Forces(4)];
   case 'momy'
      A=[(DLoadLCS(3)-DLoadLCS(6))*L^2/6;  -DLoadLCS(3)*L^2/2;   Forces(3)*L;  Forces(5)];
   case 'momz'
      A=[(DLoadLCS(2)-DLoadLCS(5))*L^2/6;  -DLoadLCS(2)*L^2/2;   Forces(2)*L;  Forces(6)];
   otherwise
      error('Unknown element force.')
end

FdiagrLCS=polyval(A,Points);

if (length(A)-find(A~=0,1))>1
   loc=roots(polyder(A));
   loc=[0; 1; loc(loc>0 & loc<1)];   
else
   loc=[0; 1];
end

Extrema=polyval(A,loc);
