function [pxyz,pind,pvalue]=patch_shell4(Nodes,NodeNum,Values)

%PATCH_SHELL4  Patch information of the shell4 elements for plotting.
%
%   [pxyz,pind,pvalue] = patch_shell4(Nodes,NodeNum,Values) returns matrices
%   to plot patches of shell4 elements.
%
%   Nodes      Node definitions        [NodID x y z]
%   NodeNum    Node numbers       [NodID1 NodID2 NodID3 NodID4] (nElem * 4)
%   Values     Values assigned to nodes used for coloring    (nElem * 4)
%   pxyz       Coordinates of Nodes                          (4*nElem * 3)
%   pind       indices of Nodes                                (nElem * 4)
%   pvalue     Values arranged per Node                      (4*nElem * 1)
%
%   See also PLOTSTRESSCONTOURF, PLOTSHELLFCONTOURF.

% Miche Jansen
% 2010

nElem=size(NodeNum,1);


pxyz=zeros(4*nElem,3);

pvalue = zeros(4*nElem,1);


for iElem=1:nElem
    for ind =1:4 
    loc=find(Nodes(:,1)==NodeNum(iElem,ind));
    if isempty(loc)
        error('Node %i is not defined.',NodeNum(iElem,1))
    elseif length(loc)>1
        error('Node %i is multiply defined.',NodeNum(iElem,1))
    else
        pxyz(4*(iElem-1)+ind,:)=Nodes(loc,2:4);
        
    end
    end
    pvalue(4*(iElem-1)+(1:4)) = Values(iElem,:); 
end


pind = reshape((1:4*nElem),4,nElem);
pind = pind.';

end
