function Forces=forceslcs_beam(KeLCS,UeLCS,DLoadLCS,L)

%FORCESLCS_BEAM   Compute the element forces for a beam element in the LCS.
%
%   Forces=forceslcs_beam(KeLCS,UeLCS,DLoadLCS,L)
%   Forces=forceslcs_beam(KeLCS,UeLCS)
%   computes the element forces for the beam element in the local coordinate 
%   system (algebraic convention).
%
%   KeLCS      Element stiffness matrix (12 * 12)
%   UeLCS      Displacements (12 * 1)
%   DLoadLCS   Distributed loads       [n1localX; n1localY; n1localZ; ...]
%   Forces     Element forces          [N; Vy; Vz; T; My; Mz](12 * 1)
%
%   See also FORCES_BEAM, FORCESLCS_TRUSS

% David Dooms
% October 2008

% compute element forces
Forces=KeLCS*UeLCS;

if nargin>2
    % subtract equivalent nodal forces (distributed loads) from element forces
    Forces=Forces-loadslcs_beam(DLoadLCS,L);
end
