function NeDLoad = nedloadlcs_beam(Points)

%NEDLOADLCS_BEAM Shape functions for a distributed load on a beam element.
%
%   NeLCS = nedloadlcs_beam(Points) determines the values of the shape functions 
%   for a distributed load in the specified points. These are used to compute 
%   the displacements that occur due to the distributed loads if all nodes are
%   fixed.
%
%   Points     Points in the local coordinate system (1 * nPoints)
%   NeDLoad    Values (nPoints * 6)
%
%   See also DISP_BEAM, NELCS_BEAM.

% David Dooms
% September 2008

Points=Points(:).';

A=[ 0  0  0  0  0  0;
   -1  5 -7  3  0  0;
   -1  5 -7  3  0  0;
    0  0  0  0  0  0;
    1  0 -3  2  0  0;
    1  0 -3  2  0  0;];

NeDLoad=zeros(size(Points,2),6);

for k=1:6
    NeDLoad(:,k)=polyval(A(k,:),Points);
end
