function [ForcesLCS,ForcesGCS]=forces_beam(Node,Section,Material,UeGCS,DLoad,Options)

%FORCES_BEAM   Compute the element forces for a beam element.
%
%   [ForcesLCS,ForcesGCS]=forces_beam(Node,Section,Material,UeGCS,DLoad,Options)
%   [ForcesLCS,ForcesGCS]=forces_beam(Node,Section,Material,UeGCS,DLoad)
%   [ForcesLCS,ForcesGCS]=forces_beam(Node,Section,Material,UeGCS)
%   computes the element forces for the beam element in the local and the  
%   global coordinate system (algebraic convention).
%
%   Node       Node definitions           [x y z] (3 * 3)
%   Section    Section definition         [A ky kz Ixx Iyy Izz]
%   Material   Material definition        [E nu]
%   UeGCS      Displacements (12 * 1)
%   DLoad      Distributed loads       [n1globalX; n1globalY; n1globalZ; ...] 
%                                                                        (6 * 1)
%   Options    Element options            {Option1 Option2 ...}
%   ForcesLCS  Element forces in the LCS (12 * 1)
%   ForcesGCS  Element forces in the GCS (12 * 1)
%
%   See also FORCESLCS_BEAM, ELEMFORCES.

% David Dooms
% October 2008

% transform displacements from global to local coordinate system
t=trans_beam(Node);
T=blkdiag(t,t,t,t);
UeLCS=T*UeGCS;

% transform distributed loads from global to local coordinate system
if nargin>4
    T=blkdiag(t,t);
    DLoadLCS=T*DLoad;
else
    DLoadLCS=zeros(6,size(UeGCS,2));
end

% compute element length
L=norm(Node(2,:)-Node(1,:));

E=Material(1);
nu=Material(2);
A=Section(1);
ky=Section(2);
kz=Section(3);
Ixx=Section(4);
Iyy=Section(5);
Izz=Section(6);

% compute element stiffness matrix in local coordinate system
KeLCS=kelcs_beam(L,A,ky,kz,Ixx,Iyy,Izz,E,nu);

% compute the element forces in the local coordinate system
ForcesLCS=forceslcs_beam(KeLCS,UeLCS,DLoadLCS,L);

% transform the element forces from local to global coordinate system
T=blkdiag(t,t,t,t);
ForcesGCS=T.'*ForcesLCS;
