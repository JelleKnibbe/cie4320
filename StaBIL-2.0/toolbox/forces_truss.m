function [ForcesLCS,ForcesGCS]=forces_truss(Node,Section,Material,UeGCS,DLoad,Options)

%FORCES_TRUSS   Compute the element forces for a truss element.
%
%   [ForcesLCS,ForcesGCS]=forces_truss(Node,Section,Material,UeGCS)
%   computes the element forces for the truss element in the local and the  
%   global coordinate system (algebraic convention).
%
%   Node       Node definitions           [x y z] (2 * 3)
%   Section    Section definition         [A]
%   Material   Material definition        [E]
%   UeGCS      Displacements (6 * 1)
%   Options    Element options            {Option1 Option2 ...}
%   ForcesLCS  Element forces in the LCS (12 * 1)
%   ForcesGCS  Element forces in the GCS (12 * 1)
%
%   See also FORCESLCS_TRUSS, ELEMFORCES.

% David Dooms
% October 2008

% transform displacements from global to local coordinate system
t=trans_truss(Node);
T=blkdiag(t,t);
UeLCS=T*UeGCS;

% compute element length
L=norm(Node(2,:)-Node(1,:));

E=Material(1);
A=Section(1);

% compute element stiffness matrix in local coordinate system
KeLCS=kelcs_truss(L,A,E);

% compute the element forces in the local coordinate system
forcesLCS1=forceslcs_truss(KeLCS,UeLCS);

% transform the element forces from local to global coordinate system
forcesGCS1=T.'*forcesLCS1;

% reorder
ForcesLCS=zeros(12,size(UeGCS,2));
ForcesLCS(1,:)=forcesLCS1(1,:);
ForcesLCS(7,:)=forcesLCS1(4,:);
ForcesGCS=zeros(12,size(UeGCS,2));
ForcesGCS(1:3,:)=forcesGCS1(1:3,:);
ForcesGCS(7:9,:)=forcesGCS1(4:6,:);
