function [Phi,Omega,M,K,DOF,S_pgen] = FE_fun_Jelle(x)
% function version of: FE_model
%
% Simplified multistorey building
% Course: OE44055, Instructor Dr. Eliz-Mari Lourens
% Dominik Fallais, May 2017
%
% Features: 
%
% Two floor groups with distinct sections, material properties and kinematic
% support conditions. Columns are uniform over the height of the structure. 

%% DEFAULT: GEOMETRY, SECTIONS, MATERIAL PROPERTIES
% Main Structural dimensions:
% ---------------------------
nFlrs=5;        % number of floors
HCol=3.5;       % column height: distance between floors 
BFlr=6;         % width of floors
nndsCol=7;      % number of (structural) nodes per column
nndsFlr=5;      % number of (structural) nodes per floor element (excluding column nodes); odd number to allow excitation at floor centre

% Section dimensions:
% - section 1: left&right column 
bCol = 0.30;        % Column section width
hCol = 0.30;        % Column section height

% - section 2: floor group I (bottom two floors)
bFlr1 = 0.30;       % beam section width
hFlr1 = 0.50;       % beam section height
kcf1  = 1;          % kinematic constraint factor

% - section 3: floor group II (top three floors)
bFlr2 = 0.25;       % beam section width
hFlr2 = 0.35;       % beam section height
kcf2  = 1E6;        % kinematic constraint factor

% Material properties:
% - material 1: left&right column 
% - concrete (M40)
E_c=40E9;         % Young's modulus
nu_c=0.25;        % Poisson's ratio
rho_c=2500;       % concrete density

% - material 2: floor group I 
% - concrete (M35)
E_f1=35E9;         % Young's modulus
nu_f1=0.25;        % Poisson's ratio
rho_f1=2500;       % concrete density
m_f1=6000;         % additional floor mass per unit length

% - material 3: floor group II 
% - concrete (M30) 
E_f2=30E9;         % Young's modulus
nu_f2=0.25;        % Poisson's ratio
rho_f2=2200;       % density
m_f2=2000;         % additional floor mass per unit length

%% ASSIGN FUNCTION INPUT by overwriting variables to be used
m_f1 = x(1);
kcf2 = x(2);


%% Floor grouping
% Define groups of floors with distinct properties: section, material, kcf_l, kcf_r.
% In case a floor or all floors is not assigned to a group [sec,mat,kcf] will be set to [2,2,1]!!
%
% floorgroups = [group#  flr_beg  flr_end   sec.    mat.    kcf_L.  kcf_R. ] 
floorgroups   = [ 1      1        2         2       2       kcf1    kcf1 ;     % floor group 1
                  2      3        nFlrs     3       3       kcf2    kcf2];     % floor group 2
 
%% NODE DEFINTION
% Nodes = [node# X Y Z]
%-----------------------
NodeXYZ = [];

% Left column nodes
for ifln = 1:nFlrs
    y0 = HCol*(ifln-1);
    ye = y0 + HCol;   
    % foundation left
    if ifln == 1; 
        NodeXYZ = [NodeXYZ; -(BFlr/2) 0, 0]; 
    end
    % column nodes left
    NodeXYZ = [NodeXYZ; [ [-(BFlr/2)*ones(1,nndsCol-1)]', [y0+(ye-y0)/(nndsCol-1):(ye-y0)/(nndsCol-1):ye]',  [0*ones(1,nndsCol-1)]']  ];
end

% Right column nodes
for ifln = 1:nFlrs
    y0 = HCol*(ifln-1);
    ye = y0 + HCol;   
    % foundation right
    if ifln == 1; 
        NodeXYZ = [NodeXYZ; +(BFlr/2) 0, 0]; 
    end
    % column nodes right
    NodeXYZ = [NodeXYZ; [ [ (BFlr/2)*ones(1,nndsCol-1)]', [y0+(ye-y0)/(nndsCol-1):(ye-y0)/(nndsCol-1):ye]', [0*ones(1,nndsCol-1)]']   ];
end

% Floor nodes
for ifln = 1:nFlrs
    y0 = HCol*(ifln-1);
    ye = y0 + HCol;   
    x0 = -(BFlr/2);
    xe = (BFlr/2);
    dx = (xe-x0)/(nndsFlr+1);
   
    NodeXYZ = [NodeXYZ; [-(BFlr/2), ye, 0]    ];
    NodeXYZ = [NodeXYZ; [[x0+dx:dx:xe-dx]' , [ye*ones(1,nndsFlr)]' , [0*ones(1,nndsFlr)]']      ];   
    NodeXYZ = [NodeXYZ; [(BFlr/2), ye, 0]     ];
end

% Define the required reference node
NodeXYZ = [NodeXYZ; [0 0 0]];

% Add node numbers: 
nNode=size(NodeXYZ,1);   % Number of nodes
NodID=[1:nNode]';        % Node numbers
Nodes=[NodID, NodeXYZ];

% Clear temporary variables
clear NodID nNode NodeXYZ y0 ye x0 xe dx 

%% TYPE DEFINTION
% Types ={EltTypID EltName Options}
%----------------------------------
Types=  {1        'beam'};

%% SECTION DEFINITION
% Sections=[SecID A ky  kz  Ixx Iyy Izz]
%----------------------------------------------
Sections=  [1     hCol*bCol   Inf Inf 0   0   hCol^3*bCol/12;      % L&R Columns
            2     hFlr1*bFlr1 Inf Inf 0   0   hFlr1^3*bFlr1/12;    % Floor group 1
            3     hFlr2*bFlr2 Inf Inf 0   0   hFlr2^3*bFlr2/12];   % Floor group 2
        
%% MATERIAL DEFINITION
% Materials=[MatID E nu rho];
%----------------------------------
Materials=  [1     E_c     nu_c    rho_c;                           % L&R column
             2     E_f1    nu_f1   rho_f1 + m_f1/(hFlr1*bFlr1);     % floor group 1    
             3     E_f2    nu_f2   rho_f2 + m_f2/(hFlr2*bFlr2)];    % floor group 2    

%% ELEMENT DEFINITION
% Elements=[EltID TypID SecID MatID n1 n2 n3]
%--------------------------------------------

% Element connectivity information: 
Ncl = 1:nFlrs*(nndsCol-1)+1;                                    % left column nodes
Ncr = Ncl(end)+1:2*Ncl(end);                                    % right column nodes
Nccl = (nndsCol):(nndsCol-1):Ncl(end);                          % left column nodes next to floor connector nodes
Nccr = Nccl(end)+1+(nndsCol-1):(nndsCol-1):Ncr(end);            % right column nodes next to floor connector nodes
Nfcl = Ncr(end)+1:(nndsFlr+2):Ncr(end)+1+(nFlrs-1)*(nndsFlr+2); % left floor connector nodes, floor nodes: Nfcl, Nfcr
Nfcr = Nfcl + (nndsFlr+2) - 1;                                  % right floor connector nodes
Nf   = Nfcl(1):Nfcr(end);                                       % floor nodes 
Nref = size(Nodes,1);                                           % reference node

% Element defintion
% Initialize element counter & element matrix
eln = 0;        % Element number counter
Elements = [];  % array containing element definitions
Ecl = [];       % array to keep track which elements are in left colum
Ecr = [];       % array to keep track which elements are in right column
Efl = [];       % 2D array to keep track which elements are in floors

% (1) Define elements between nodes of left Columns
for i = 1:length(Ncl)-1;
    eln = eln + 1;
    Ecl = [Ecl, eln]; 
    Elements(eln,:) = [eln,  1,    1,    1,    Ncl(i), Ncl(i+1), Nref ];
end

% (2) Define  elements between nodes of right Columns
for i = 1:length(Ncr)-1;
    eln = eln + 1;
    Ecr = [Ecr, eln]; 
    Elements(eln,:) = [eln,  1,    1,    1,    Ncr(i), Ncr(i+1), Nref ];    
end      

% (3) Define  elements between floor nodes 
% floorgroups   = [nr  f_beg  f_end    sec    mat   kcf ] 
floornodes = reshape(Nf,(nndsFlr+2),nFlrs)';
for i = 1:size(floornodes,1)            % for number of floors   
    % check whether this floor belongs to a group 
    gindx = find([i>=floorgroups(:,2)].*[i<=floorgroups(:,3)]);

    if ~isempty(gindx) %--------------------> if floor is in group:
        for j = 1:size(floornodes,2)-1      % for all elements in floor
           eln = eln + 1;
           Efl(i,j) = eln; 
           Elements(eln,:) = [eln,  1,    floorgroups(gindx,4), floorgroups(gindx,5), floornodes(i,j), floornodes(i,j+1), Nref ];
        end
    else %----------------------------------> if the floor is not in a group:
        for j = 1:size(floornodes,2)-1      % for all elements in floor
           eln = eln + 1;
           Efl(i,j) = eln; 
           Elements(eln,:) = [eln,  1,    2,    2,    floornodes(i,j), floornodes(i,j+1), Nref ];
        end
        disp(['ERR?! - elem. def.: floor nr. ',num2str(i), ': does not belong to a group!'])
    end
end

%% BOUNDARY CONDITIONS AND ASSEMBLY
%----------------------------------
% - 2D analysis: select only UX,UY,ROTZ
% - clamp / remove all dof at bottom: node 1 and Ncr(1) 
seldof=[0.03; 0.04; 0.05; 1; Ncr(1)]; 
DOF=getdof(Elements,Types); 
DOF=removedof(DOF,seldof);
nDOF=length(DOF);

% ASSEMBLY OF UNCONSTRAINED M and K
[K,M]=asmkm(Nodes,Elements,Types,Sections,Materials,DOF);

%% DEFINE FORCE SELECTION MATRICES (S_p)
%---------------------------------------
% Allocate forces in space - allocated forces need to be consistent with 
% kinematic constraints. In order to be able to perform a dynamic simulation 
% using different forces, a number of force definitions (more than needed)
% will be pre-processed. These definitions will be combined with the
% 1) unconstrained (K,M) matrices and 2) the constrained matrices in order to
% find the correct matrices for (K,M,S_p)
%
% Nodal loads
% - N1(i/a) :    nodes at column-to-floor connections (master nodes only!)
% - N2      :    vertical nodal force at center of floor 4
% - N3(i/a) :    individual or equal forces at floor centers
% Distributed loads
% - D1(i/a) :    horizontal load on all/individual left column elements
% 
% Subscripts
% - i: individual row vector per element/node -> one force per element/node
% - a: one force for all elements/nodes
%
% information on sizes and indices for components of selection matrices:
% content:     [S_pN1i,  S_pN1a,  S_pN2, S_pN3i,   S_pN3a,  S_pD1i,               S_pD1a]
% indices:     [(:,1:5), (:,6),   (:,7), (:,8:12), (:,13),  (:,14:43),            (:,44)] 
% size: ndof X [nFlrs,   1,        1,    nFlrs,     1,      nFlrs*(nndsCol-1),    1     ] 

% N1): vertical forces at nodes of column to floor connections (master nodes only!)
nodes  = Nccl ;                             % nodes  
seldof = nodes + 0.01;                      % define dof type
S_pN1i = [selectdof(DOF,seldof)]';          % individual: selection matrix: [ndof x nFlrs]
S_pN1a = sum(S_pN1i,2);                     % all in one: selection matrix: [ndof x 1]

% N2): individual vertical force at floor center of 5th floor
S_pN2 = [selectdof(DOF,94.02)]';            % individual node: [ndof x 1]

% N3): individual vertical forces at floor center
nodes  = Nfcl + round(((nndsFlr+2)-1)/2);   % [1 x nd : robust]
seldof = nodes + 0.02;                      % define dof type     
S_pN3i = [selectdof(DOF,seldof)]';          % individual: selection matrix: [ndof x nFlrs]
S_pN3a = sum(S_pN3i,2);                     % all in one: selection matrix: [ndof x 1]

% D1): one horizontal load for all left column elements 
DLoads = [Ecl',repmat( [1   0   0,   1   0   0]  ,length(Ecl),1)];     % define load profile: all elements same load 
S_pD1a  = elemloads(DLoads,Nodes,Elements,Types,DOF);                  % [ndof x 1]

% D2): individual horizontal loads for all left column elements
S_pD1i = [];
for i = 1:size(Ecl,2)
    DLoads  = [Ecl(i)',[1   0   0,   1   0   0]];                      % define load profile per element
    S_pD1i(:,i) = elemloads(DLoads,Nodes,Elements,Types,DOF);          % allocate row-wise in matrix  [ndof x 30]  
end

S_pgenunc = [S_pN1i, S_pN1a, S_pN2, S_pN3i, S_pN3a, S_pD1i, S_pD1a];

%% KINEMATIC CONSTRAINT CONDITIONS
%---------------------------------
% The kinematic constraints connect the outer floor nodes to the columns. 
% A kinematic constraint equation is prescribed which linearly relates the 
% kinematics of the column-dof's to the floor-dof's. Note that the X and Y
% displacements of the outer floor nodes and the column nodes have to be
% identical. The kinematic constraint equation and the corresponding factor 
%(kcf) will be used to model flexibility in the rotational dof only. 
%
% Constr = [Constant CoefS SlaveDOF CoefM1 MasterDOF1 ...]
% values for "CoefS": slave coefficient
%  - kcf = 1        : rigid connection / clamped 
%  - kcf = inf      : hinged (but will produce warning - use large number 
%                       e.g. 1E8 instead)
%  - kcf = <1..inf> : more realistic case: something between clamped and hinged
%--------------------------------------------------------------------------
 
% Kinematic constraint on left connector nodes 
nce = 0;    % initialise constraint counter
% per floor - make switch for groups
for i = 1:length(Nccl);   
    % check whether this floor belongs to a group 
    gindx = find([i>=floorgroups(:,2)].*[i<=floorgroups(:,3)]);
    if ~isempty(gindx) %--------------------> if floor is in group:
        nce = nce + 1; Constr(nce,:) = [0,  1,                    Nfcl(i)+0.01,    -1,   Nccl(i)+0.01 ];
        nce = nce + 1; Constr(nce,:) = [0,  1,                    Nfcl(i)+0.02,    -1,   Nccl(i)+0.02 ];        
        nce = nce + 1; Constr(nce,:) = [0,  floorgroups(gindx,6), Nfcl(i)+0.06,    -1,   Nccl(i)+0.06 ]; 
    else %----------------------------------> if the floor is not in a group:
        nce = nce + 1; Constr(nce,:) = [0,  1,  Nfcl(i)+0.01,    -1,   Nccl(i)+0.01 ];
        nce = nce + 1; Constr(nce,:) = [0,  1,  Nfcl(i)+0.02,    -1,   Nccl(i)+0.02 ];        
        nce = nce + 1; Constr(nce,:) = [0,  1,  Nfcl(i)+0.06,    -1,   Nccl(i)+0.06 ]; 
        disp(['ERR?! - kcf left: floor nr. ',num2str(i), 'does not belong to a group!'])
    end
end  

% Kinematic constraint on right connector nodes
for i = 1:length(Nccr);
    % check whether this floor belongs to a group 
    gindx = find([i>=floorgroups(:,2)].*[i<=floorgroups(:,3)]);
    if ~isempty(gindx) %--------------------> if floor is in group:
        nce = nce + 1; Constr(nce,:) = [0,  1,                    Nfcr(i)+0.01,    -1,   Nccr(i)+0.01 ];
        nce = nce + 1; Constr(nce,:) = [0,  1,                    Nfcr(i)+0.02,    -1,   Nccr(i)+0.02 ];    
        nce = nce + 1; Constr(nce,:) = [0,  floorgroups(gindx,7), Nfcr(i)+0.06,   -1,   Nccr(i)+0.06 ];  
    else %----------------------------------> if the floor is not in a group:
        nce = nce + 1; Constr(nce,:) = [0,  1,  Nfcr(i)+0.01,    -1,   Nccr(i)+0.01 ];
        nce = nce + 1; Constr(nce,:) = [0,  1,  Nfcr(i)+0.02,    -1,   Nccr(i)+0.02 ];        
        nce = nce + 1; Constr(nce,:) = [0,  1,  Nfcr(i)+0.06,    -1,   Nccr(i)+0.06 ]; 
        disp(['ERR?! - kcf right: floor nr. ',num2str(i), 'does not belong to a group!'])
    end 
end  

% Constrain stiffness, mass and force allocation matrices
% Combine unconstrained M and K matrix with constraint relations and
% general force allocation matrix to obtain:
%--------------------------------------------------------------------------
[K,S_pgen,M] = addconstr(Constr,DOF,K,S_pgenunc,M);

%% SOLVE EIGENVALUE PROBLEM
nModes = length(K)-size(Constr,1);    % number of non-singular entries in Omega
[Phi,Omega]=eigfem(K,M,nModes);       % solve Phi and Omega



