FE_model_Updated;
load("Accelerations_B.mat"); % ACCELERATIONS NOT DISPLACEMENTS!!!!
load("Forces.mat");

n_p = 2;
n_d = 11;

S_daero = S_d(1:6,:);
S_dwind = S_d(7:11,:);

% dn = dn - mean(dn,2); % detrend?
% numerical integration?
dwind = dn(7:11,:);
daero = dn(1:6,:);

S_pwind = S_p(:,1);
S_paero = S_p(:,2);

dt = 0.01;
N = 2048;
F = 1/dt;
df = F/N;
freq = [0:N/2-1,-N/2:-1]*df;

dt = 0.01;
N = N;
F = 1/dt;
df = F/N;
freq = [0:N/2-1,-N/2:-1]*df;
   
dnw = fft(dn,[],2);
dwindw = fft(dwind,[],2);
daerow = fft(daero,[],2);
for ind = 1:n_d
    % Time domain
    figure;
    subplot(2,1,1);
    plot(t,dn(ind,:)); hold on; 
    % Frequency domain
    subplot(2,1,2);
    semilogy(freq(1:N/2),abs(dnw(ind,1:N/2))); hold on;
end

pw = fft(p,[],2);

% Construct the damping matrix for proportional damping
xi = 0.02;  % Constant modal damping ratio assumed
Cstar = diag(2*Omega(1:8)*xi);
C = M*Phi(:,1:8)*Cstar*Phi(:,1:8)'*M;

L_s=eye(1);

for ind = 1:N/2
    omega = freq(ind)*2*pi;
    bwindw = dwindw / (omega+0.001)^2;
    baerow = daerow / (omega+0.001)^2;
    H = inv(-omega^2*M+1i*omega*C+K);
    FRF(ind) = S_pwind'*H*S_pwind;
    Awind = S_dwind*H*S_pwind;
    Aaero = S_daero*H*S_paero;
    A = S_d*H*S_p;
    lambdawind(ind) = 1e0*trace(transpose(Awind)*Awind)/trace(transpose(L_s)*L_s);
    lambdaaero(ind) = 1e0*trace(transpose(Aaero)*Aaero)/trace(transpose(L_s)*L_s);
    if freq(ind) > 3
        p_LSw(1,ind) = inv(transpose(Awind)*(Awind)) * transpose(Awind) * bwindw(:,ind);
        p_LSw(2,ind) = inv(transpose(Aaero)*(Aaero)) * transpose(Aaero) * baerow(:,ind);
        p_TIKHw(1,ind) = inv(transpose(Awind)*Awind + lambdawind(ind)^2*transpose(L_s)*L_s)*transpose(Awind)*bwindw(:,ind);
        p_TIKHw(2,ind) = inv(transpose(Aaero)*Aaero + lambdaaero(ind)^2*transpose(L_s)*L_s)*transpose(Aaero)*baerow(:,ind);
        Cond(ind) = cond(Awind);
    else
        p_LSw(1,ind) = 0;
        p_LSw(2,ind) = 0;
        p_TIKHw(1,ind) = 0;
        p_TIKHw(2,ind) = 0;
        Cond(ind) = cond(Awind);
    end
end

p_LSw = [p_LSw zeros(n_p,1) conj(p_LSw(:,end:-1:2))];
p_LS = ifft(p_LSw,[],2);

p_TIKHw = [p_TIKHw zeros(n_p,1) conj(p_TIKHw(:,end:-1:2))];
p_TIKH = ifft(p_TIKHw,[],2);

figure;
semilogy(freq(1:N/2),Cond);
title("Cond(A)");

figure;
semilogy(freq(1:N/2),abs(lambdawind));
title("lambdawind");

figure;
semilogy(freq(1:N/2),abs(lambdaaero));
title("lambdaaero");

figure;
semilogy(freq(1:N/2),abs(FRF));
title("FRF");

for ind = 1:n_p
    % Time domain
    figure;
    subplot(2,1,1);
    sgtitle(force_names(ind))
    plot(t,p_LS(ind,:)); hold on;
    plot(t,p_TIKH(ind,:)); hold on;
    plot(t,p(ind,:)); 
    legend("Least-Squares", "Tikhonov", "True");
    % Frequency domain
    subplot(2,1,2);
    semilogy(freq(1:N/2),abs(p_LSw(ind,1:N/2))); hold on;
    semilogy(freq(1:N/2),abs(p_TIKHw(ind,1:N/2))); hold on;
    semilogy(freq(1:N/2),abs(pw(ind,1:N/2)));
    semilogy(freq(1:N/2),abs(p_LSw(ind,1:N/2))./abs(pw(ind,1:N/2)));
    legend("Least-Squares", "Tikhonov", "True", "Division");
end