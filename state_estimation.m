% 1 - Loading forces and adding noise
load("Accelerations_B.mat");
load("Forces.mat");

n_p = 2;
n_d = 11;

% Add noise --> dn
nl = 0.05;
for ind = 1:n_d
	noise = nl*max(dn(ind,:))*randn(1,N);
    dn1(ind,:) = dn(ind,:) + noise;
end

dt = t(2);

% Forces - Can be tikh or from forces.mat
p1 = p_TIKH;
p2 = p_LS;
p3 = p;

% nl = 0.10;
% for ind = 1:n_p
% 	noise1 = nl*max(p1(ind,:))*randn(1,N);
%     p1(ind,:) = p1(ind,:) + noise1;
% end

% Construct the damping matrix for proportional damping
xi = 0.02;  % Constant modal damping ratio assumed
Cstar = diag(2.*Omega.*xi);
C = M*Phi*Cstar*Phi'*M;

% 2 - Constructing matrix A,B,C,D
% S_p1 = [S_pD1a , S_pN2];
S_p1 = S_p;

S_a1 = S_d;
S_v1 = zeros(n_d,nDOF);
S_d1 = zeros(n_d,nDOF);

% Setting up the Modal analysis
KK = Phi'*K*Phi;
CC = Phi'*C*Phi;

sz = size(Phi);
Nreduced = sz(2);

% ASSEMBLE SYSTEM MATRIX A
A_c = [zeros(Nreduced,Nreduced) eye(Nreduced) ; -KK -CC ];
A = expm(A_c*dt);

% ASSEMBLE INPUT MATRIX B
B_c = [zeros(Nreduced,n_p) ; Phi'*S_p1];
B = (A-eye(2.*Nreduced))*inv(A_c)*B_c;

% ASSEMBLE OUTPUT MATRIX C
Cc = [S_d1*Phi-S_a1*Phi*KK S_v1*Phi-S_a1*Phi*CC];

% ASSEMBLE DIRECT TRANSMISSION MATRIX D
D = S_a1*Phi*Phi'*S_p1;

Q = 1*10^(-10) .* eye(2*Nreduced);                                % Process noise covariance matrix
Q2 = 1*10^(-10) .* eye(2*Nreduced);                                % Process noise covariance matrix
R = 1*10^(-3) .* eye(n_d);                                       % Measurement noise covariance matrix (estimate)  
R2 = 1*10^(-3) .* eye(n_d);                                       % Measurement noise covariance matrix (estimate) 
x0 = zeros(2.*Nreduced,1);                          % Initial state vector
P0 = 1*10^(-10) .* eye(2*Nreduced);                               % Initial state error covariance matrix
% P0 = zeros(2*Nreduced,2*Nreduced);                    % Initial state error covariance matrix - Idea: Erorr is 0 because structure has zero initial conditions
[x_filter,P_filter,Kgain] = kalman_filter(A,B,Cc,D,Q,R,P0,x0,p1,dn1);
% 
% [x2_filter,P2_filter,Kgain2] = kalman_filter(A,B,Cc,D,Q2,R2,P0,x0,p2,dn1);
% 
% [x3_filter,P3_filter,Kgain3] = kalman_filter(A,B,Cc,D,Q,R,P0,x0,p3,dn1);

S_phi = [Phi zeros(nDOF,Nreduced)];
u_filter = S_phi*x_filter;

% u2_filter = S_phi*x2_filter;

% Frequency Response functions of modes
% Parameters
dt = t(2);
size_t = size(t);
N = size_t(2);
F = 1/dt;
df = F/N;
freq =[0:N/2-1,-N/2:-1]*df; 

xw_mode1 = fft(x_filter(1,:),[],2);  
xw_mode2 = fft(x_filter(2,:),[],2); 

% xw2_mode1 = fft(x2_filter(1,:),[],2);  
% xw2_mode2 = fft(x2_filter(2,:),[],2); 
% 
% xw3_mode1 = fft(x3_filter(1,:),[],2);  
% xw3_mode2 = fft(x3_filter(2,:),[],2); 

% 4) Plot
% Modal displacements

% figure;
% plot(t,x_filter(1,:),"-r",t,x2_filter(1,:),"--b",t,x3_filter(1,:),"-g");
% title('Modal response of mode 1');
% xlabel('Time [s]');
% ylabel('Modal response');
% % legend('Q = 1e-10','Q = 1e10');
% % legend('R = 1e-3','R = 1e3');
% legend('TIKH','LS','Control');
% 
% figure;
% plot(t,x_filter(2,:),"-r",t,x2_filter(2,:),"--b",t,x3_filter(2,:),"-g");
% title('Modal response of mode 2');
% xlabel('Time [s]');
% ylabel('Modal response');
% % legend('Q = 1e-10','Q = 1e10');
% % legend('R = 1e-3','R = 1e3');
% legend('TIKH','LS','Control');
% 
% figure;
% plot(freq(1:N/2),abs(xw_mode1(1,1:N/2)).*dt,"-r",freq(1:N/2),abs(xw2_mode1(1,1:N/2))*dt,"--b",freq(1:N/2),abs(xw3_mode1(1,1:N/2)).*dt,"-g");
% title('Frequency Response function of mode 1');
% xlabel('Frequency [Hz]');
% ylabel('FRF');
% axis([0 5 -inf inf]);
% legend('TIKH','LS','Control');
% 
% figure;
% plot(freq(1:N/2),abs(xw_mode2(1,1:N/2)).*dt,"-r",freq(1:N/2),abs(xw2_mode2(1,1:N/2)).*dt,"--b",freq(1:N/2),abs(xw3_mode2(1,1:N/2)).*dt,"-g");
% title('Frequency Response function of mode 2');
% xlabel('Frequency [Hz]');
% ylabel('FRF');
% axis([0 5 -inf inf]);
% legend('TIKH','LS','Control');

% for ind = 1:15
%     Knorm(ind) = norm(Kgain(:,:,ind));
%     Knorm2(ind) = norm(Kgain2(:,:,ind));
% %     Pnorm(ind) = norm(P_filter(:,:,ind));
% end
% 
% n = linspace(1,15,15);
% 
% figure;
% plot(n,Knorm,n,Knorm2);
% xlabel('Iteration');
% ylabel('norm(Kk)');
% % legend('Q = 1e-10','Q = 1e10');
% legend('R = 1e-3','R = 1e3');

% figure;
% plot(n,Pnorm);


% % Real displacements
% % Horizontal displacement DOF 10
% c10=selectdof(DOF,[10.01]);
% S_10 = [c10];
% figure;
% plot(t,S_10*u_filter);
% title('Horizontal displacement DOF 10');
% xlabel('Time [s]');
% ylabel('Displacement [m]');
% % legend("Original", "Optimised");
% 
% % Horizontal displacement DOF 22
% c22=selectdof(DOF,[22.01]);
% S_22 = [c22];
% figure;
% plot(t,S_22*u_filter);
% title('Horizontal displacement DOF 22');
% xlabel('Time [s]');
% ylabel('Displacement [m]');
% % legend("Original", "Optimised");
% 
% % Horizontal displacement DOF 28
% c28=selectdof(DOF,[28.01]);
% S_28 = [c28];
% figure;
% plot(t,S_28*u_filter);
% title('Horizontal displacement DOF 28');
% xlabel('Time [s]');
% ylabel('Displacement [m]');
% % legend("Original", "Optimised");
% 
% % Vertical displacement DOF 66
% c66 = selectdof(DOF,[66.02]);
% S_66 = [c66];
% figure;
% plot(t,S_66*u_filter);
% title('Vertical displacement DOF 66');
% xlabel('Time [s]');
% ylabel('Displacement [m]');
% 
% % Vertical displacement DOF 73
% c73 = selectdof(DOF,[73.02]);
% S_73 = [c73];
% figure;
% plot(t,S_73*u_filter);
% title('Vertical displacement DOF 73');
% xlabel('Time [s]');
% ylabel('Displacement [m]');
% 
% % Vertical displacement DOF 94
% c94 = selectdof(DOF,[94.02]);
% S_94 = [c94];
% figure;
% plot(t,S_94*u_filter);
% title('Vertical displacement DOF 94');
% xlabel('Time [s]');
% ylabel('Displacement [m]');

% Accelerations




