clear;
load("Accelerations_B.mat");

dt = 0.01;
N = 2048;
F = 1/dt;
df = F/N;
n_d = 11;
freq = [0:N/2-1,-N/2:-1]*df;

dn = ones(N);

nl = 0.05;
for ind = 1:n_d
	noise = nl*max(dn(ind,:))*randn(1,N);
    dn1(ind,:) = dn(ind,:) + noise;
end

rxx = xcorr(dn1(1,:));

sxx = fft(rxx,[],2);

sx = trapz(sxx);
var = sqrt(sx);